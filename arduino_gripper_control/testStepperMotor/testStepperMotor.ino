

#define STEPPER_1_ENABLE_PIN      2  
#define STEPPER_1_RESET_PIN       3
#define STEPPER_1_SLEEP_PIN       4
#define STEPPER_1_PWM_PIN         5
#define STEPPER_1_DIRECTION_PIN   7

#define STEPPER_2_ENABLE_PIN      A3
#define STEPPER_2_RESET_PIN       A2
#define STEPPER_2_SLEEP_PIN       A1
#define STEPPER_2_PWM_PIN         6
#define STEPPER_2_DIRECTION_PIN   A0

#define STEPPER_1                 1
#define STEPPER_2                 2
#define CLOCKWISE_STEPPER_DIR     1
#define ANTICLOCKWISE_STEPPER_DIR  2

#define RESET_DELAY               500 //Microseconds
#define SLEEP_AWAKE_DELAY         1000 //Microseconds. See Alegro A4988 datasheet
                                    //for details

//setupPins function sets the output pins of the device.
void setupPins()
{
  //Stepper Driver 1
  pinMode(STEPPER_1_ENABLE_PIN, OUTPUT);
  pinMode(STEPPER_1_RESET_PIN, OUTPUT);
  pinMode(STEPPER_1_SLEEP_PIN, OUTPUT);
  pinMode(STEPPER_1_PWM_PIN, OUTPUT);
  pinMode(STEPPER_1_DIRECTION_PIN, OUTPUT);

  //Stepper Driver 2
  pinMode(STEPPER_2_ENABLE_PIN, OUTPUT);
  pinMode(STEPPER_2_RESET_PIN, OUTPUT);
  pinMode(STEPPER_2_SLEEP_PIN, OUTPUT);
  pinMode(STEPPER_2_PWM_PIN, OUTPUT);
  pinMode(STEPPER_2_DIRECTION_PIN, OUTPUT);

  //Set initial pin output values of drivers

}

//resetStepper performs the reset operation upon the 
//Motor driver
void resetStepper(uint8_t stepperMotor)
{
  if (stepperMotor == STEPPER_1)
  {
    digitalWrite(STEPPER_1_RESET_PIN, LOW);  
    delayMicroseconds(RESET_DELAY);
    digitalWrite(STEPPER_1_RESET_PIN, HIGH);
  }
  else if (stepperMotor == STEPPER_2)
  {
    digitalWrite(STEPPER_2_RESET_PIN, LOW);
    delayMicroseconds(RESET_DELAY);
    digitalWrite(STEPPER_2_RESET_PIN, HIGH);
  }
}

//enableStepper motor activates the stepper motor driver
void enableStepper(uint8_t stepperMotor, bool enableStepper)
{
  if (stepperMotor == STEPPER_1)
  {
    if (enableStepper)
    {
      digitalWrite(STEPPER_1_ENABLE_PIN, LOW);
    }
    else
    {
      digitalWrite(STEPPER_1_ENABLE_PIN, HIGH);
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (enableStepper)
    {
      digitalWrite(STEPPER_2_ENABLE_PIN, LOW);
    }
    else
    {
      digitalWrite(STEPPER_2_ENABLE_PIN, HIGH);
    }
  }
}

//sleepStepper function puts the stepper motor driver to sleep and
//to awake the stepper when the system wakes.
void sleepStepper(uint8_t stepperMotor, bool enableSleep)
{
  if (stepperMotor == STEPPER_1)
  {
    if (enableSleep)
    {
      digitalWrite(STEPPER_1_SLEEP_PIN, LOW);
    }
    else
    {
      digitalWrite(STEPPER_1_SLEEP_PIN, HIGH);
      delayMicroseconds(SLEEP_AWAKE_DELAY);
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (enableSleep)
    {
      digitalWrite(STEPPER_2_SLEEP_PIN, LOW);
    }
    else
    {
      digitalWrite(STEPPER_2_SLEEP_PIN, HIGH);
      delayMicroseconds(SLEEP_AWAKE_DELAY);
    }
  }
}

void changeDirectionStepper(uint8_t stepperMotor, uint8_t stepperMotorDirection)
{
  if (stepperMotor == STEPPER_1)
  {
    if (stepperMotorDirection == CLOCKWISE_STEPPER_DIR)
    {
      digitalWrite(STEPPER_1_DIRECTION_PIN, HIGH);
    }
    else
    {
      digitalWrite(STEPPER_1_DIRECTION_PIN, LOW);    
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (stepperMotorDirection == CLOCKWISE_STEPPER_DIR)
    {
      digitalWrite(STEPPER_2_DIRECTION_PIN, HIGH);
    }
    else
    {
      digitalWrite(STEPPER_2_DIRECTION_PIN, LOW);    
    }
  }
}

void turnStepper(uint8_t stepperMotor, uint8_t speed)
{
  if (stepperMotor == STEPPER_1)
  {
    analogWrite(STEPPER_1_PWM_PIN, speed);
  }
  else if (stepperMotor == STEPPER_2)
  {
    analogWrite(STEPPER_2_PWM_PIN, speed);
  }

}

void setup()
{
  setupPins();
  digitalWrite(STEPPER_1_RESET_PIN,HIGH);
  digitalWrite(STEPPER_2_RESET_PIN,HIGH);

  enableStepper(STEPPER_1, true);
  enableStepper(STEPPER_2, true);

  sleepStepper(STEPPER_1, false);
  sleepStepper(STEPPER_2, false);
  changeDirectionStepper(STEPPER_1, CLOCKWISE_STEPPER_DIR);
}

void loop()
{
  digitalWrite(STEPPER_2_PWM_PIN, HIGH);
  //digitalWrite(STEPPER_1_PWM_PIN, HIGH);
  delayMicroseconds(16000);
  digitalWrite(STEPPER_2_PWM_PIN, LOW);
  //digitalWrite(STEPPER_1_PWM_PIN, LOW);
  delayMicroseconds(16000);

 if (millis() > 6000)
  { 
changeDirectionStepper(STEPPER_2, ANTICLOCKWISE_STEPPER_DIR);
  }

  if (millis() > 10000)
 {
     changeDirectionStepper(STEPPER_2, CLOCKWISE_STEPPER_DIR);
}
}
