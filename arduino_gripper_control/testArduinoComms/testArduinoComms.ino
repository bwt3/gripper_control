//Simple program for testing ROS communications with PC.
//Author: Bruce Thomson
  
#include <stdlib.h>
#include <ros.h>
#include <gripper_control/ArduinoCommand.h>
#include <std_msgs/String.h>
#include <gripper_control/ArduinoStatus.h>
#include "ArduinoDef.h"
#include "ArduinoCommandDef.h"
#include "A4988.h"
#include <std_msgs/UInt16.h>

//Scheduling
long previousTime = 0;

//A4988
A4988 stepperMotors = A4988();

void commandMessageHandler(const gripper_control::ArduinoCommand &msg);


//Internal State
uint8_t current_command             = WAIT_COMMAND;
uint8_t nut_stepper_direction       = 0;
uint16_t nut_stepper_angle           = 0;
uint8_t nut_stepper_speed           = 0;
uint8_t nut_stepper_rotations       = 0;
uint8_t gripper_stepper_direction   = 0;
uint8_t gripper_stepper_angle       = 0;
uint8_t gripper_stepper_speed       = 0;
uint8_t gripper_stepper_rotations   = 0;
bool electromagnetActive            = false;

//ROS Stuff
ros::NodeHandle node;

gripper_control::ArduinoStatus status;
std_msgs::UInt16                debug_status;

ros::Publisher publisher("/testLibrary/PC_Arduino_TX", &status);
ros::Publisher debug_pub("ARDUINO_DEBUG", &debug_status);
ros::Subscriber<gripper_control::ArduinoCommand> subscriber("/testLibrary/PC_Arduino_RX" , &commandMessageHandler);

//commandMessageHandler Function is the function which is called
//when a command message is received. This performs decoding of
//the message etc.
void commandMessageHandler(const gripper_control::ArduinoCommand &msg)
{
  //Decode current command and run approprate functions
  switch (msg.current_command)
  {
    case (ELECTROMAGNET_COMMAND):
      current_command = ELECTROMAGNET_COMMAND;
      performElectromagnetCommand(msg);
    case (NUT_STEPPER_MOVEMENT_COMMAND):
      current_command = NUT_STEPPER_MOVEMENT_COMMAND;
      performNutStepperMotorCommand(msg);
    case (GRIPPER_STEPPER_MOVEMENT_COMMAND):
      current_command = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      performGripperStepperMotorCommand(msg);
  } 
}

//performElectromagnetCommand function switches on and off the electromagnet
bool performElectromagnetCommand(const gripper_control::ArduinoCommand &msg)
{
  current_command = ELECTROMAGNET_COMMAND;
  if (msg.electromagnet)
  {
    digitalWrite(ELECTROMAGNET_PIN, HIGH);
    electromagnetActive = true;
    A4988_Status * currentStatusNut = stepperMotors.getStepperStatus(STEPPER_1);
    A4988_Status * currentStatusGripper = stepperMotors.getStepperStatus(STEPPER_2);

    currentStatusNut->targetAngle = 0;
    currentStatusNut->targetNumberOfRotations = 0;
    currentStatusNut->currentAngle = 0;
    currentStatusNut->currentSpeed = 0;
    currentStatusNut->enabled = true;
    
    currentStatusGripper->targetAngle = 0;
    currentStatusGripper->currentAngle = 0;
    currentStatusGripper->targetNumberOfRotations = 0;
    currentStatusGripper->currentSpeed = 0;
    currentStatusGripper->enabled = true;
  }
  else
  {
    digitalWrite(ELECTROMAGNET_PIN, LOW);
    electromagnetActive = false;
  }
  current_command = WAIT_COMMAND;
  return true;
}

bool performNutStepperMotorCommand(const gripper_control::ArduinoCommand &msg)
{
  A4988_Status * currentStatusNut = stepperMotors.getStepperStatus(STEPPER_1);
  if ((msg.nut_stepper_enable) && (currentStatusNut->enabled == true))
  {
    stepperMotors.enableStepper(STEPPER_1, true);
    stepperMotors.sleepStepper(STEPPER_1, true);
    stepperMotors.setSpeed(STEPPER_1, msg.nut_stepper_speed);
    stepperMotors.setTargetAngle(STEPPER_1, msg.nut_stepper_angle, msg.nut_stepper_rotations);
    stepperMotors.changeDirection(STEPPER_1, msg.nut_stepper_direction);
    stepperMotors.sleepStepper(STEPPER_1, false);
  }
  else if ((msg.nut_stepper_enable) && (currentStatusNut->enabled == false))
  {
    stepperMotors.enableStepper(STEPPER_1, true);
  }
  else
  {
    stepperMotors.enableStepper(STEPPER_1, false);
  }

  
  debug_status.data = msg.nut_stepper_speed;

  debug_pub.publish(&debug_status);
  return true;
}

bool performGripperStepperMotorCommand(const gripper_control::ArduinoCommand &msg)
{
  A4988_Status * currentGripperStatus = stepperMotors.getStepperStatus(STEPPER_2);
  if ((msg.gripper_stepper_enable) && (currentGripperStatus->enabled == true))
  {
    stepperMotors.enableStepper(STEPPER_2, true);
    stepperMotors.sleepStepper(STEPPER_2, true);
    stepperMotors.setSpeed(STEPPER_2, msg.gripper_stepper_speed);
    stepperMotors.setTargetAngle(STEPPER_2, msg.gripper_stepper_angle, msg.gripper_stepper_rotations);
    stepperMotors.changeDirection(STEPPER_2, msg.gripper_stepper_direction);
    stepperMotors.sleepStepper(STEPPER_2, false);
  }
  else if ((msg.gripper_stepper_enable) && (currentGripperStatus->enabled == false))
  {
    stepperMotors.enableStepper(STEPPER_2, true);
  }
  else
  {
    stepperMotors.enableStepper(STEPPER_2, false);
  }
  debug_status.data = msg.gripper_stepper_rotations;

  debug_pub.publish(&debug_status);
}

//setupPins function sets all the required I/O direction
void setupPins()
{
  pinMode(ELECTROMAGNET_PIN, OUTPUT);
}

void setup()
{
  setupPins();
  stepperMotors.init();
  node.initNode();
  node.subscribe(subscriber);
  node.advertise(publisher);
  node.advertise(debug_pub);
}

void loop()
{
  long currentTime = millis();

  stepperMotors.stepperMotorLoop();
  if (currentTime >= previousTime + ROS_LOOP_RATE)
  {
    publishCurrentStatus();
    node.spinOnce();    
    previousTime = millis();
  }

  node.spinOnce();
  delayMicroseconds(1);
}

//publishCurrentStatus function writes out the current status of the Arduino to
//the PC uses the status object.
bool publishCurrentStatus()
{
  A4988_Status * currentStatusNut = stepperMotors.getStepperStatus(STEPPER_1);
  A4988_Status * currentStatusGripper = stepperMotors.getStepperStatus(STEPPER_2);

  if (currentStatusNut->targetNumberOfRotations == 0)
  {
    current_command = WAIT_COMMAND;
  }
  status.current_command                        = current_command;

  status.nut_stepper_direction                  = currentStatusNut->currentDirection;
  status.nut_stepper_current_angle              = currentStatusNut->currentAngle;
  status.nut_stepper_target_angle               = currentStatusNut->targetAngle;
  status.nut_stepper_number_of_rotations_to_go  = currentStatusNut->targetNumberOfRotations;
  status.nut_stepper_speed                      = currentStatusNut->currentSpeed;
  status.nut_stepper_enabled                    = currentStatusNut->enabled;

  
  status.gripper_stepper_direction                 = currentStatusGripper->currentDirection;
  status.gripper_stepper_current_angle             = currentStatusGripper->currentAngle;
  status.gripper_stepper_target_angle              = currentStatusGripper->targetAngle;
  status.gripper_stepper_number_of_rotations_to_go = currentStatusGripper->targetNumberOfRotations;
  status.gripper_stepper_speed                     = currentStatusGripper->currentSpeed;
  status.gripper_stepper_enabled                   = currentStatusGripper->enabled;

  
  
  publisher.publish(&status);
}