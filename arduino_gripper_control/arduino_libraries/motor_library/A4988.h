//Filename: A4988.h
//Author:   Bruce Thomson
//Purpose:  A4899 Stepper Motor Driver Header File. Designed
//          to be used with ROS and allow for multitasking on the Stepper.
//          Assumes 2 Stepper Motors are in operation but the code can be 
//          extended for multiple stepper motors.

//Arduino Uno Pin Defines 
#define STEPPER_1_ENABLE_PIN      2  
#define STEPPER_1_RESET_PIN       3
#define STEPPER_1_SLEEP_PIN       4
#define STEPPER_1_PWM_PIN         5
#define STEPPER_1_DIRECTION_PIN   7

#define STEPPER_2_ENABLE_PIN      A3
#define STEPPER_2_RESET_PIN       A2
#define STEPPER_2_SLEEP_PIN       A1
#define STEPPER_2_PWM_PIN         6
#define STEPPER_2_DIRECTION_PIN   A0

//Defining stepper motor and direction
#define STEPPER_1                 1
#define STEPPER_2                 2
#define CLOCKWISE_STEPPER_DIR     1
#define ANTICLOCKWISE_STEPPER_DIR 2

#define NUMBER_OF_MOTOR_STEPS     200
#define ANGLE_OF_STEP             18
#define CLOCKWISE_LIMIT           3582
#define ANTI_CLOCKWISE_LIMIT      0     

//Delays for Reset and awaking from sleep.
#define RESET_DELAY               500 //Microseconds
#define SLEEP_AWAKE_DELAY         1000 //Microseconds. See Alegro A4988 datasheet
                                    //for details

#include "Arduino.h"
#include "A4988_Status.h"             //Include the Status variables for the motor;

class A4988
{
  public:

  A4988();

  void init();
  A4988_Status * getStepperStatus(uint8_t stepperMotor);
  void stepperMotorLoop();
  void resetStepper(uint8_t stepperMotor);
  void enableStepper(uint8_t stepperMotor, bool enableStepper);
  void sleepStepper(uint8_t stepperMotor, bool enableSleep);
  void changeDirection(uint8_t stepperMotor, uint8_t stepperMotorDirection);
  void setTargetAngle(uint8_t stepperMotor, uint16_t angle, uint8_t rotations);
  void setSpeed(uint8_t stepperMotor, uint16_t speed);



  private:
  void singleStepperMotorLoop(A4988_Status * motorStatus);
  void updatePosition(A4988_Status * motorStatus);
  void setupPins();


};