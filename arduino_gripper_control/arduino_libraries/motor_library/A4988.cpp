#include "A4988.h"

A4988_Status stepper1Status;
A4988_Status stepper2Status;

//A4988 Constructor
A4988::A4988()
{

}

void A4988::init()
{
  setupPins();
  resetStepper(STEPPER_1);
  resetStepper(STEPPER_2);

  enableStepper(STEPPER_1, true);
  enableStepper(STEPPER_2, true);

  sleepStepper(STEPPER_1, false);
  sleepStepper(STEPPER_2, false);


  stepper1Status.PWMPin       = STEPPER_1_PWM_PIN;
  stepper2Status.PWMPin       = STEPPER_2_PWM_PIN;
  stepper1Status.currentAngle = 0;
  stepper2Status.currentAngle = 0;
}


A4988_Status * A4988::getStepperStatus(uint8_t stepperMotor)
{
  if (stepperMotor == STEPPER_1)
  {
    return &stepper1Status;
  }
  else if (stepperMotor == STEPPER_2)
  {
    return &stepper2Status;
  }
  return NULL;
}


//setupPins function sets the output pins of the device.
void A4988::setupPins()
{
  //Stepper Driver 1
  pinMode(STEPPER_1_ENABLE_PIN, OUTPUT);
  pinMode(STEPPER_1_RESET_PIN, OUTPUT);
  pinMode(STEPPER_1_SLEEP_PIN, OUTPUT);
  pinMode(STEPPER_1_PWM_PIN, OUTPUT);
  pinMode(STEPPER_1_DIRECTION_PIN, OUTPUT);

  //Stepper Driver 2
  pinMode(STEPPER_2_ENABLE_PIN, OUTPUT);
  pinMode(STEPPER_2_RESET_PIN, OUTPUT);
  pinMode(STEPPER_2_SLEEP_PIN, OUTPUT);
  pinMode(STEPPER_2_PWM_PIN, OUTPUT);
  pinMode(STEPPER_2_DIRECTION_PIN, OUTPUT);
}

//resetStepper performs the reset operation upon the 
//Motor driver
void A4988::resetStepper(uint8_t stepperMotor)
{
  if (stepperMotor == STEPPER_1)
  {
    digitalWrite(STEPPER_1_RESET_PIN, LOW);  
    delayMicroseconds(RESET_DELAY);
    digitalWrite(STEPPER_1_RESET_PIN, HIGH);
  }
  else if (stepperMotor == STEPPER_2)
  {
    digitalWrite(STEPPER_2_RESET_PIN, LOW);
    delayMicroseconds(RESET_DELAY);
    digitalWrite(STEPPER_2_RESET_PIN, HIGH);
  }
}

//enableStepper motor activates the stepper motor driver
void A4988::enableStepper(uint8_t stepperMotor, bool enableStepper)
{
  if (stepperMotor == STEPPER_1)
  {
    if (enableStepper)
    {
      digitalWrite(STEPPER_1_ENABLE_PIN, LOW);
      stepper1Status.enabled = true;
      
    }
    else
    {
      digitalWrite(STEPPER_1_ENABLE_PIN, HIGH);
      stepper2Status.enabled = false;
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (enableStepper)
    {
      digitalWrite(STEPPER_2_ENABLE_PIN, LOW);
      stepper2Status.enabled = true;
    }
    else
    {
      digitalWrite(STEPPER_2_ENABLE_PIN, HIGH);
      stepper2Status.enabled = false;
    }
  }
}

//sleepStepper function puts the stepper motor driver to sleep and
//to awake the stepper when the system wakes.
void A4988::sleepStepper(uint8_t stepperMotor, bool enableSleep)
{
  if (stepperMotor == STEPPER_1)
  {
    if (enableSleep)
    {
      digitalWrite(STEPPER_1_SLEEP_PIN, LOW);
      stepper1Status.sleep = true;
    }
    else
    {
      digitalWrite(STEPPER_1_SLEEP_PIN, HIGH);
      stepper1Status.sleep = false;
      delayMicroseconds(SLEEP_AWAKE_DELAY); //Delay required before driving a PWM into the system. 
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (enableSleep)
    {
      digitalWrite(STEPPER_2_SLEEP_PIN, LOW);
      stepper2Status.sleep = true;
    }
    else
    {
      digitalWrite(STEPPER_2_SLEEP_PIN, HIGH);
      stepper2Status.sleep = false;
      delayMicroseconds(SLEEP_AWAKE_DELAY);
    }
  }
}

//changeDirectionStepper changes the direction of the stepper.
void A4988::changeDirection(uint8_t stepperMotor, uint8_t stepperMotorDirection)
{
  if (stepperMotor == STEPPER_1)
  {
    if (stepperMotorDirection == CLOCKWISE_STEPPER_DIR)
    {
      digitalWrite(STEPPER_1_DIRECTION_PIN, HIGH);
      stepper1Status.currentDirection = CLOCKWISE_STEPPER_DIR;
    }
    else
    {
      digitalWrite(STEPPER_1_DIRECTION_PIN, LOW);
      stepper1Status.currentDirection = ANTICLOCKWISE_STEPPER_DIR;
    }
  }
  else if (stepperMotor == STEPPER_2)
  {
    if (stepperMotorDirection == CLOCKWISE_STEPPER_DIR)
    {
      digitalWrite(STEPPER_2_DIRECTION_PIN, HIGH);
      stepper2Status.currentDirection = CLOCKWISE_STEPPER_DIR;
    }
    else
    {
      digitalWrite(STEPPER_2_DIRECTION_PIN, LOW); 
      stepper2Status.currentDirection = ANTICLOCKWISE_STEPPER_DIR;   
    }
  }
  //Serial.println("Direction Changed");
}

//setTargetAngle function sets the target angle and the number of rotations required
//to achieve the angle.
void A4988::setTargetAngle(uint8_t stepperMotor, uint16_t angle, uint8_t rotations)
{
  if (angle % ANGLE_OF_STEP != 0)
  {
    return;
  }
  if (stepperMotor == STEPPER_1)
  {
    stepper1Status.targetAngle = angle;
    stepper1Status.targetNumberOfRotations = rotations;
  }
  else if (stepperMotor == STEPPER_2)
  {
    stepper2Status.targetAngle = angle;
    stepper2Status.targetNumberOfRotations = rotations;
  }
}

//setSpeed function takes a RPM value in and will set the PWM to the stepper motor driver
//to the correct speed
void A4988::setSpeed(uint8_t stepperMotor, uint16_t speed)
{
  double rotationPerSecond;
  double ticksPerSecond;
  double timePerPulseS;
  double timeHighPulseS;
  long   timeHighPulseMs;
  bool   isPulseTimeUs;
  
  //Serial.print("The speed in RPM is: ");
  //Serial.println(speed, DEC);

  rotationPerSecond = (double) speed / 60;
  ticksPerSecond    = rotationPerSecond * NUMBER_OF_MOTOR_STEPS;
  timePerPulseS    =  1.0 /ticksPerSecond ;
  timeHighPulseS   = timePerPulseS   / 2;

  //Serial.print("The time per rotation in seconds is ");
  //Serial.println(timePerRotationS, DEC);

  //Serial.print("The time per pulse in seconds is ");
  //Serial.println(timePerPulseS, DEC);

  //Serial.print("The time per high pulse in seconds is ");
  //Serial.println(timeHighPulseS, DEC);


  timeHighPulseS = timeHighPulseS * 1000;
  //Serial.print("The time per high pulse in milliseconds is (double)");
  //Serial.println(timeHighPulseS, DEC);

  timeHighPulseMs = long(timeHighPulseS);

  //Serial.print("The time per high pulse in milliseconds is (int)");
  //Serial.println(timeHighPulseMs, DEC);

  isPulseTimeUs = false;

  if (timeHighPulseMs == 0  || (timeHighPulseS < 16)) //Included to make any speed which requires a pulse less than 16ms to use the microseconds function
  {
    timeHighPulseS = timeHighPulseS * 1000;
    timeHighPulseMs = long(timeHighPulseS);
    isPulseTimeUs = true;
    //Serial.print("The time per high pulse in microseconds is (int)");
    //Serial.println(timeHighPulseMs, DEC);
  }

  if (stepperMotor == STEPPER_1)
  {
    stepper1Status.pulseHighTime  = timeHighPulseMs;
    stepper1Status.isPulseTimeUs  = isPulseTimeUs;
    stepper1Status.timeOfLastStep = 0;
  }
  else if (stepperMotor == STEPPER_2)
  {
    stepper2Status.pulseHighTime = timeHighPulseMs;
    stepper2Status.isPulseTimeUs = isPulseTimeUs;
    stepper2Status.timeOfLastStep = 0;
  }
  //Serial.print("The Pulse High Time is:");
  //Serial.println(timeHighPulseMs, DEC);
}

//updatePosition sets 
void A4988::updatePosition(A4988_Status * motorStatus)
{
  uint16_t currentAngle = motorStatus->currentAngle;
  uint16_t targetAngle  = motorStatus->targetAngle;
  uint8_t  currentDirection = motorStatus->currentDirection;
  
  if (currentAngle == targetAngle)
  {
    motorStatus->targetNumberOfRotations = motorStatus->targetNumberOfRotations - 1;
  }

  if (currentDirection == CLOCKWISE_STEPPER_DIR)
  {
    if (currentAngle == CLOCKWISE_LIMIT)
    {
      motorStatus->currentAngle = ANTI_CLOCKWISE_LIMIT;
    }
    else 
    {
      motorStatus->currentAngle = currentAngle + ANGLE_OF_STEP;
    }
  }
  else
  {
    if (currentAngle == ANTI_CLOCKWISE_LIMIT)
    {
      motorStatus->currentAngle = CLOCKWISE_LIMIT;
    }
    else 
    {
      motorStatus->currentAngle = currentAngle - ANGLE_OF_STEP;
    }
  }

  //Serial.print("Current Angle");
  //Serial.println(motorStatus->currentAngle);
}

//stepperMotorLoop function MUST be called within the loop() function
// of the main Arduino sketch.
void A4988::stepperMotorLoop()
{
  singleStepperMotorLoop(&stepper1Status);
  singleStepperMotorLoop(&stepper2Status);
}

void A4988::singleStepperMotorLoop(A4988_Status * motorStatus)
{
  //When the stepper motor driver is in sleep mode or not enabled then simply returns
  if (motorStatus->sleep == true || motorStatus->enabled == false)
  {
    return;
  }
  //Serial.print("The current rotations: ");
  //Serial.println(motorStatus->targetNumberOfRotations, DEC);

  if ((motorStatus->targetAngle == motorStatus->currentAngle) && (motorStatus->targetNumberOfRotations == 0))
  {
    return;
  }

  long currentTime;
  if (motorStatus->isPulseTimeUs)
  {
    currentTime = micros();

    if (currentTime >= (motorStatus->timeOfLastStep + motorStatus->pulseHighTime))
    {

      //Serial.print("Motor set:");
      ////Serial.println(motorStatus->currentPinState, DEC);
      motorStatus->currentPinState = !motorStatus->currentPinState;
      digitalWrite(motorStatus->PWMPin, motorStatus->currentPinState);

      if (motorStatus->currentPinState)
      {
        ////Serial.println("Entered Set Pin");
        updatePosition(motorStatus);
      }

      motorStatus->timeOfLastStep = micros();
    }
  }
  else
  {
    currentTime = millis();
    if (currentTime >= (motorStatus->timeOfLastStep + motorStatus->pulseHighTime))
    {
      ////Serial.println("Entered Set Pin");
      motorStatus->currentPinState = !motorStatus->currentPinState;
      digitalWrite(motorStatus->PWMPin, motorStatus->currentPinState);
      
      if (motorStatus->currentPinState)
      {
        updatePosition(motorStatus);
      }

      motorStatus->timeOfLastStep = millis();
    }
  }
}