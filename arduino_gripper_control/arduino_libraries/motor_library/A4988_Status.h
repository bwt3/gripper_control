//Filename: A4988_Status.h
//Author:   Bruce Thomson
//Purpose:  A4899 Stepper Motor Driver Status Struct Header File.
//          This is a struct containing the current status of the
//          stepper motor assosiated with it. Allows for passing
//          back of status variables to the main Arduino sketch


struct A4988_Status{
  uint8_t   PWMPin;
  bool      currentPinState;
  uint16_t  currentAngle;
  uint16_t  targetAngle;
  uint8_t   targetNumberOfRotations;
  uint8_t   currentDirection;
  uint16_t  currentSpeed; //In RPM
  bool      isPulseTimeUs;         //If True then the pulse time is in Microseconds. Else the pulse time is in Milliseconds.
  long      pulseHighTime;
  long      timeOfLastStep;
  bool      enabled;      //When enabled the stepper motor is locked in position
  bool      sleep;        //Sleep states whether the motor driver is in the low power sleep mode
};

typedef struct A4988_Status A4988_Status;