#ifndef _ROS_SERVICE_SetPathParameters_h
#define _ROS_SERVICE_SetPathParameters_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

static const char SETPATHPARAMETERS[] = "iiwa_msgs/SetPathParameters";

  class SetPathParametersRequest : public ros::Msg
  {
    public:
      float joint_relative_velocity;
      float joint_relative_acceleration;
      float override_joint_acceleration;

    SetPathParametersRequest():
      joint_relative_velocity(0),
      joint_relative_acceleration(0),
      override_joint_acceleration(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->joint_relative_velocity);
      offset += serializeAvrFloat64(outbuffer + offset, this->joint_relative_acceleration);
      offset += serializeAvrFloat64(outbuffer + offset, this->override_joint_acceleration);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->joint_relative_velocity));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->joint_relative_acceleration));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->override_joint_acceleration));
     return offset;
    }

    const char * getType(){ return SETPATHPARAMETERS; };
    const char * getMD5(){ return "7e28d392b765001fa175af1a9d6aedcd"; };

  };

  class SetPathParametersResponse : public ros::Msg
  {
    public:
      bool success;
      const char* error;

    SetPathParametersResponse():
      success(0),
      error("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      uint32_t length_error = strlen(this->error);
      memcpy(outbuffer + offset, &length_error, sizeof(uint32_t));
      offset += 4;
      memcpy(outbuffer + offset, this->error, length_error);
      offset += length_error;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t length_error;
      memcpy(&length_error, (inbuffer + offset), sizeof(uint32_t));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_error; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_error-1]=0;
      this->error = (char *)(inbuffer + offset-1);
      offset += length_error;
     return offset;
    }

    const char * getType(){ return SETPATHPARAMETERS; };
    const char * getMD5(){ return "45872d25d65c97743cc71afc6d4e884d"; };

  };

  class SetPathParameters {
    public:
    typedef SetPathParametersRequest Request;
    typedef SetPathParametersResponse Response;
  };

}
#endif
