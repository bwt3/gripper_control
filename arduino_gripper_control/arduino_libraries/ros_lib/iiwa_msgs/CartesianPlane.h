#ifndef _ROS_iiwa_msgs_CartesianPlane_h
#define _ROS_iiwa_msgs_CartesianPlane_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class CartesianPlane : public ros::Msg
  {
    public:
      enum { XY =  1 };
      enum { XZ =  2 };
      enum { YZ =  3 };

    CartesianPlane()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/CartesianPlane"; };
    const char * getMD5(){ return "15617e8d13d4abd04145334685da37ba"; };

  };

}
#endif