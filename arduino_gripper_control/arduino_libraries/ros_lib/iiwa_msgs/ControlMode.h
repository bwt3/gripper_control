#ifndef _ROS_iiwa_msgs_ControlMode_h
#define _ROS_iiwa_msgs_ControlMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class ControlMode : public ros::Msg
  {
    public:
      enum { POSITION_CONTROL =  0 };
      enum { JOINT_IMPEDANCE =   1 };
      enum { CARTESIAN_IMPEDANCE =  2 };
      enum { DESIRED_FORCE =  3 };
      enum { SINE_PATTERN =  4 };

    ControlMode()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/ControlMode"; };
    const char * getMD5(){ return "fbf6f4c74f01882c27df4152be6af2d9"; };

  };

}
#endif