#ifndef _ROS_iiwa_msgs_JointStiffness_h
#define _ROS_iiwa_msgs_JointStiffness_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointStiffness : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::JointQuantity stiffness;

    JointStiffness():
      header(),
      stiffness()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->stiffness.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->stiffness.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointStiffness"; };
    const char * getMD5(){ return "36eb539f80d8414d77a80b821ac39758"; };

  };

}
#endif