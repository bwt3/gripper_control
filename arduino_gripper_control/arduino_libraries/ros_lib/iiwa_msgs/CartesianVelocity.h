#ifndef _ROS_iiwa_msgs_CartesianVelocity_h
#define _ROS_iiwa_msgs_CartesianVelocity_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/CartesianQuantity.h"

namespace iiwa_msgs
{

  class CartesianVelocity : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::CartesianQuantity velocity;

    CartesianVelocity():
      header(),
      velocity()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->velocity.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->velocity.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/CartesianVelocity"; };
    const char * getMD5(){ return "e7362f328353be8a6cf8b8ff5cd15e56"; };

  };

}
#endif