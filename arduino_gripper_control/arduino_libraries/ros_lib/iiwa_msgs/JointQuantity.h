#ifndef _ROS_iiwa_msgs_JointQuantity_h
#define _ROS_iiwa_msgs_JointQuantity_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class JointQuantity : public ros::Msg
  {
    public:
      float a1;
      float a2;
      float a3;
      float a4;
      float a5;
      float a6;
      float a7;

    JointQuantity():
      a1(0),
      a2(0),
      a3(0),
      a4(0),
      a5(0),
      a6(0),
      a7(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_a1;
      u_a1.real = this->a1;
      *(outbuffer + offset + 0) = (u_a1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a1);
      union {
        float real;
        uint32_t base;
      } u_a2;
      u_a2.real = this->a2;
      *(outbuffer + offset + 0) = (u_a2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a2);
      union {
        float real;
        uint32_t base;
      } u_a3;
      u_a3.real = this->a3;
      *(outbuffer + offset + 0) = (u_a3.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a3.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a3.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a3.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a3);
      union {
        float real;
        uint32_t base;
      } u_a4;
      u_a4.real = this->a4;
      *(outbuffer + offset + 0) = (u_a4.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a4.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a4.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a4.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a4);
      union {
        float real;
        uint32_t base;
      } u_a5;
      u_a5.real = this->a5;
      *(outbuffer + offset + 0) = (u_a5.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a5.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a5.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a5.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a5);
      union {
        float real;
        uint32_t base;
      } u_a6;
      u_a6.real = this->a6;
      *(outbuffer + offset + 0) = (u_a6.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a6.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a6.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a6.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a6);
      union {
        float real;
        uint32_t base;
      } u_a7;
      u_a7.real = this->a7;
      *(outbuffer + offset + 0) = (u_a7.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_a7.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_a7.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_a7.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->a7);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        float real;
        uint32_t base;
      } u_a1;
      u_a1.base = 0;
      u_a1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a1 = u_a1.real;
      offset += sizeof(this->a1);
      union {
        float real;
        uint32_t base;
      } u_a2;
      u_a2.base = 0;
      u_a2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a2 = u_a2.real;
      offset += sizeof(this->a2);
      union {
        float real;
        uint32_t base;
      } u_a3;
      u_a3.base = 0;
      u_a3.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a3.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a3.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a3.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a3 = u_a3.real;
      offset += sizeof(this->a3);
      union {
        float real;
        uint32_t base;
      } u_a4;
      u_a4.base = 0;
      u_a4.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a4.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a4.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a4.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a4 = u_a4.real;
      offset += sizeof(this->a4);
      union {
        float real;
        uint32_t base;
      } u_a5;
      u_a5.base = 0;
      u_a5.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a5.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a5.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a5.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a5 = u_a5.real;
      offset += sizeof(this->a5);
      union {
        float real;
        uint32_t base;
      } u_a6;
      u_a6.base = 0;
      u_a6.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a6.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a6.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a6.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a6 = u_a6.real;
      offset += sizeof(this->a6);
      union {
        float real;
        uint32_t base;
      } u_a7;
      u_a7.base = 0;
      u_a7.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_a7.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_a7.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_a7.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->a7 = u_a7.real;
      offset += sizeof(this->a7);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointQuantity"; };
    const char * getMD5(){ return "b9f90cf50b6e4af396f731df7da11689"; };

  };

}
#endif