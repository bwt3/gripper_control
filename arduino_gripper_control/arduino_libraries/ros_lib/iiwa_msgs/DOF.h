#ifndef _ROS_iiwa_msgs_DOF_h
#define _ROS_iiwa_msgs_DOF_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class DOF : public ros::Msg
  {
    public:
      enum { X =  1 };
      enum { Y =  2 };
      enum { Z =  3 };

    DOF()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/DOF"; };
    const char * getMD5(){ return "0f8911033659687c381ba21b896e7a05"; };

  };

}
#endif