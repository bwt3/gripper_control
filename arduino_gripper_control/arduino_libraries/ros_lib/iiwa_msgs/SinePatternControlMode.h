#ifndef _ROS_iiwa_msgs_SinePatternControlMode_h
#define _ROS_iiwa_msgs_SinePatternControlMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class SinePatternControlMode : public ros::Msg
  {
    public:
      int32_t cartesian_dof;
      float frequency;
      float amplitude;
      float stiffness;

    SinePatternControlMode():
      cartesian_dof(0),
      frequency(0),
      amplitude(0),
      stiffness(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_cartesian_dof;
      u_cartesian_dof.real = this->cartesian_dof;
      *(outbuffer + offset + 0) = (u_cartesian_dof.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_cartesian_dof.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_cartesian_dof.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_cartesian_dof.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->cartesian_dof);
      offset += serializeAvrFloat64(outbuffer + offset, this->frequency);
      offset += serializeAvrFloat64(outbuffer + offset, this->amplitude);
      offset += serializeAvrFloat64(outbuffer + offset, this->stiffness);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_cartesian_dof;
      u_cartesian_dof.base = 0;
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->cartesian_dof = u_cartesian_dof.real;
      offset += sizeof(this->cartesian_dof);
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->frequency));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->amplitude));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->stiffness));
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/SinePatternControlMode"; };
    const char * getMD5(){ return "e72785e47bec423cb99a63dd32ae2a54"; };

  };

}
#endif