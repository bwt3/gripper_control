#ifndef _ROS_SERVICE_ConfigureSmartServo_h
#define _ROS_SERVICE_ConfigureSmartServo_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "iiwa_msgs/DesiredForceControlMode.h"
#include "iiwa_msgs/CartesianImpedanceControlMode.h"
#include "iiwa_msgs/SinePatternControlMode.h"
#include "iiwa_msgs/CartesianControlModeLimits.h"
#include "iiwa_msgs/JointImpedanceControlMode.h"

namespace iiwa_msgs
{

static const char CONFIGURESMARTSERVO[] = "iiwa_msgs/ConfigureSmartServo";

  class ConfigureSmartServoRequest : public ros::Msg
  {
    public:
      int32_t control_mode;
      iiwa_msgs::JointImpedanceControlMode joint_impedance;
      iiwa_msgs::CartesianImpedanceControlMode cartesian_impedance;
      iiwa_msgs::DesiredForceControlMode desired_force;
      iiwa_msgs::SinePatternControlMode sine_pattern;
      iiwa_msgs::CartesianControlModeLimits limits;

    ConfigureSmartServoRequest():
      control_mode(0),
      joint_impedance(),
      cartesian_impedance(),
      desired_force(),
      sine_pattern(),
      limits()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_control_mode;
      u_control_mode.real = this->control_mode;
      *(outbuffer + offset + 0) = (u_control_mode.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_control_mode.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_control_mode.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_control_mode.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->control_mode);
      offset += this->joint_impedance.serialize(outbuffer + offset);
      offset += this->cartesian_impedance.serialize(outbuffer + offset);
      offset += this->desired_force.serialize(outbuffer + offset);
      offset += this->sine_pattern.serialize(outbuffer + offset);
      offset += this->limits.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_control_mode;
      u_control_mode.base = 0;
      u_control_mode.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_control_mode.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_control_mode.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_control_mode.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->control_mode = u_control_mode.real;
      offset += sizeof(this->control_mode);
      offset += this->joint_impedance.deserialize(inbuffer + offset);
      offset += this->cartesian_impedance.deserialize(inbuffer + offset);
      offset += this->desired_force.deserialize(inbuffer + offset);
      offset += this->sine_pattern.deserialize(inbuffer + offset);
      offset += this->limits.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return CONFIGURESMARTSERVO; };
    const char * getMD5(){ return "5e02ca4e5bba97a102b6f5d4c0a7fc99"; };

  };

  class ConfigureSmartServoResponse : public ros::Msg
  {
    public:
      bool success;
      const char* error;

    ConfigureSmartServoResponse():
      success(0),
      error("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      uint32_t length_error = strlen(this->error);
      memcpy(outbuffer + offset, &length_error, sizeof(uint32_t));
      offset += 4;
      memcpy(outbuffer + offset, this->error, length_error);
      offset += length_error;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t length_error;
      memcpy(&length_error, (inbuffer + offset), sizeof(uint32_t));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_error; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_error-1]=0;
      this->error = (char *)(inbuffer + offset-1);
      offset += length_error;
     return offset;
    }

    const char * getType(){ return CONFIGURESMARTSERVO; };
    const char * getMD5(){ return "45872d25d65c97743cc71afc6d4e884d"; };

  };

  class ConfigureSmartServo {
    public:
    typedef ConfigureSmartServoRequest Request;
    typedef ConfigureSmartServoResponse Response;
  };

}
#endif
