#ifndef _ROS_iiwa_msgs_CartesianControlModeLimits_h
#define _ROS_iiwa_msgs_CartesianControlModeLimits_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "iiwa_msgs/CartesianQuantity.h"

namespace iiwa_msgs
{

  class CartesianControlModeLimits : public ros::Msg
  {
    public:
      iiwa_msgs::CartesianQuantity max_path_deviation;
      iiwa_msgs::CartesianQuantity max_control_force;
      bool max_control_force_stop;
      iiwa_msgs::CartesianQuantity max_cartesian_velocity;

    CartesianControlModeLimits():
      max_path_deviation(),
      max_control_force(),
      max_control_force_stop(0),
      max_cartesian_velocity()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->max_path_deviation.serialize(outbuffer + offset);
      offset += this->max_control_force.serialize(outbuffer + offset);
      union {
        bool real;
        uint8_t base;
      } u_max_control_force_stop;
      u_max_control_force_stop.real = this->max_control_force_stop;
      *(outbuffer + offset + 0) = (u_max_control_force_stop.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->max_control_force_stop);
      offset += this->max_cartesian_velocity.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->max_path_deviation.deserialize(inbuffer + offset);
      offset += this->max_control_force.deserialize(inbuffer + offset);
      union {
        bool real;
        uint8_t base;
      } u_max_control_force_stop;
      u_max_control_force_stop.base = 0;
      u_max_control_force_stop.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->max_control_force_stop = u_max_control_force_stop.real;
      offset += sizeof(this->max_control_force_stop);
      offset += this->max_cartesian_velocity.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/CartesianControlModeLimits"; };
    const char * getMD5(){ return "36b3a99aeab262cf60ecef9129ccb529"; };

  };

}
#endif