#ifndef _ROS_iiwa_msgs_JointVelocity_h
#define _ROS_iiwa_msgs_JointVelocity_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointVelocity : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::JointQuantity velocity;

    JointVelocity():
      header(),
      velocity()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->velocity.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->velocity.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointVelocity"; };
    const char * getMD5(){ return "efbc089b284716f4747aa2e483b101e1"; };

  };

}
#endif