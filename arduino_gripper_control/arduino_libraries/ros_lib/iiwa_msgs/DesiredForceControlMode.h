#ifndef _ROS_iiwa_msgs_DesiredForceControlMode_h
#define _ROS_iiwa_msgs_DesiredForceControlMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

  class DesiredForceControlMode : public ros::Msg
  {
    public:
      int32_t cartesian_dof;
      float desired_force;
      float desired_stiffness;

    DesiredForceControlMode():
      cartesian_dof(0),
      desired_force(0),
      desired_stiffness(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_cartesian_dof;
      u_cartesian_dof.real = this->cartesian_dof;
      *(outbuffer + offset + 0) = (u_cartesian_dof.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_cartesian_dof.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_cartesian_dof.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_cartesian_dof.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->cartesian_dof);
      offset += serializeAvrFloat64(outbuffer + offset, this->desired_force);
      offset += serializeAvrFloat64(outbuffer + offset, this->desired_stiffness);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        int32_t real;
        uint32_t base;
      } u_cartesian_dof;
      u_cartesian_dof.base = 0;
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_cartesian_dof.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->cartesian_dof = u_cartesian_dof.real;
      offset += sizeof(this->cartesian_dof);
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->desired_force));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->desired_stiffness));
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/DesiredForceControlMode"; };
    const char * getMD5(){ return "da11717a4c7e94e66d2e956ead0bf6f3"; };

  };

}
#endif