#ifndef _ROS_iiwa_msgs_JointPositionVelocity_h
#define _ROS_iiwa_msgs_JointPositionVelocity_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointPositionVelocity : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::JointQuantity position;
      iiwa_msgs::JointQuantity velocity;

    JointPositionVelocity():
      header(),
      position(),
      velocity()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->position.serialize(outbuffer + offset);
      offset += this->velocity.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->position.deserialize(inbuffer + offset);
      offset += this->velocity.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointPositionVelocity"; };
    const char * getMD5(){ return "08519eea0692f4458e7be483616eb8cd"; };

  };

}
#endif