#ifndef _ROS_iiwa_msgs_CartesianImpedanceControlMode_h
#define _ROS_iiwa_msgs_CartesianImpedanceControlMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "iiwa_msgs/CartesianQuantity.h"

namespace iiwa_msgs
{

  class CartesianImpedanceControlMode : public ros::Msg
  {
    public:
      iiwa_msgs::CartesianQuantity cartesian_stiffness;
      iiwa_msgs::CartesianQuantity cartesian_damping;
      float nullspace_stiffness;
      float nullspace_damping;

    CartesianImpedanceControlMode():
      cartesian_stiffness(),
      cartesian_damping(),
      nullspace_stiffness(0),
      nullspace_damping(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->cartesian_stiffness.serialize(outbuffer + offset);
      offset += this->cartesian_damping.serialize(outbuffer + offset);
      offset += serializeAvrFloat64(outbuffer + offset, this->nullspace_stiffness);
      offset += serializeAvrFloat64(outbuffer + offset, this->nullspace_damping);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->cartesian_stiffness.deserialize(inbuffer + offset);
      offset += this->cartesian_damping.deserialize(inbuffer + offset);
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->nullspace_stiffness));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->nullspace_damping));
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/CartesianImpedanceControlMode"; };
    const char * getMD5(){ return "7c03442e16efac806b59fb2795f102eb"; };

  };

}
#endif