#ifndef _ROS_SERVICE_TimeToDestination_h
#define _ROS_SERVICE_TimeToDestination_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace iiwa_msgs
{

static const char TIMETODESTINATION[] = "iiwa_msgs/TimeToDestination";

  class TimeToDestinationRequest : public ros::Msg
  {
    public:

    TimeToDestinationRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
     return offset;
    }

    const char * getType(){ return TIMETODESTINATION; };
    const char * getMD5(){ return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class TimeToDestinationResponse : public ros::Msg
  {
    public:
      float remaining_time;

    TimeToDestinationResponse():
      remaining_time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->remaining_time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->remaining_time));
     return offset;
    }

    const char * getType(){ return TIMETODESTINATION; };
    const char * getMD5(){ return "b71b59cce98847d0bd0310eb052e8f3f"; };

  };

  class TimeToDestination {
    public:
    typedef TimeToDestinationRequest Request;
    typedef TimeToDestinationResponse Response;
  };

}
#endif
