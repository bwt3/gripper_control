#ifndef _ROS_iiwa_msgs_CartesianEulerPose_h
#define _ROS_iiwa_msgs_CartesianEulerPose_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/CartesianQuantity.h"

namespace iiwa_msgs
{

  class CartesianEulerPose : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::CartesianQuantity pose;

    CartesianEulerPose():
      header(),
      pose()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->pose.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->pose.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/CartesianEulerPose"; };
    const char * getMD5(){ return "007ae8f5dcbe93f7ab42a1d24885796d"; };

  };

}
#endif