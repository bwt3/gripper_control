#ifndef _ROS_iiwa_msgs_JointTorque_h
#define _ROS_iiwa_msgs_JointTorque_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointTorque : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::JointQuantity torque;

    JointTorque():
      header(),
      torque()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->torque.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->torque.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointTorque"; };
    const char * getMD5(){ return "8a43c6408fbfc30d42559acdf6c2c73c"; };

  };

}
#endif