#ifndef _ROS_iiwa_msgs_JointPosition_h
#define _ROS_iiwa_msgs_JointPosition_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointPosition : public ros::Msg
  {
    public:
      std_msgs::Header header;
      iiwa_msgs::JointQuantity position;

    JointPosition():
      header(),
      position()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->position.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->position.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointPosition"; };
    const char * getMD5(){ return "658fb21894857f175855c02859cc4745"; };

  };

}
#endif