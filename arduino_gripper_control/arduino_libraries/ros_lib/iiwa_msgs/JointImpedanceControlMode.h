#ifndef _ROS_iiwa_msgs_JointImpedanceControlMode_h
#define _ROS_iiwa_msgs_JointImpedanceControlMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "iiwa_msgs/JointQuantity.h"

namespace iiwa_msgs
{

  class JointImpedanceControlMode : public ros::Msg
  {
    public:
      iiwa_msgs::JointQuantity joint_stiffness;
      iiwa_msgs::JointQuantity joint_damping;

    JointImpedanceControlMode():
      joint_stiffness(),
      joint_damping()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->joint_stiffness.serialize(outbuffer + offset);
      offset += this->joint_damping.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->joint_stiffness.deserialize(inbuffer + offset);
      offset += this->joint_damping.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "iiwa_msgs/JointImpedanceControlMode"; };
    const char * getMD5(){ return "c2267019473eba8c0a4619b649192c0d"; };

  };

}
#endif