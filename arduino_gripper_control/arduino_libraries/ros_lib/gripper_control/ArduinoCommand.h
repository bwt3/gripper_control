#ifndef _ROS_gripper_control_ArduinoCommand_h
#define _ROS_gripper_control_ArduinoCommand_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace gripper_control
{

  class ArduinoCommand : public ros::Msg
  {
    public:
      uint8_t current_command;
      uint8_t nut_stepper_direction;
      uint16_t nut_stepper_angle;
      uint8_t nut_stepper_speed;
      uint8_t nut_stepper_rotations;
      bool nut_stepper_enable;
      uint8_t gripper_stepper_direction;
      uint16_t gripper_stepper_angle;
      uint8_t gripper_stepper_speed;
      uint8_t gripper_stepper_rotations;
      bool gripper_stepper_enable;
      bool electromagnet;

    ArduinoCommand():
      current_command(0),
      nut_stepper_direction(0),
      nut_stepper_angle(0),
      nut_stepper_speed(0),
      nut_stepper_rotations(0),
      nut_stepper_enable(0),
      gripper_stepper_direction(0),
      gripper_stepper_angle(0),
      gripper_stepper_speed(0),
      gripper_stepper_rotations(0),
      gripper_stepper_enable(0),
      electromagnet(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->current_command >> (8 * 0)) & 0xFF;
      offset += sizeof(this->current_command);
      *(outbuffer + offset + 0) = (this->nut_stepper_direction >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_direction);
      *(outbuffer + offset + 0) = (this->nut_stepper_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->nut_stepper_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->nut_stepper_angle);
      *(outbuffer + offset + 0) = (this->nut_stepper_speed >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_speed);
      *(outbuffer + offset + 0) = (this->nut_stepper_rotations >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_rotations);
      union {
        bool real;
        uint8_t base;
      } u_nut_stepper_enable;
      u_nut_stepper_enable.real = this->nut_stepper_enable;
      *(outbuffer + offset + 0) = (u_nut_stepper_enable.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_enable);
      *(outbuffer + offset + 0) = (this->gripper_stepper_direction >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_direction);
      *(outbuffer + offset + 0) = (this->gripper_stepper_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->gripper_stepper_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->gripper_stepper_angle);
      *(outbuffer + offset + 0) = (this->gripper_stepper_speed >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_speed);
      *(outbuffer + offset + 0) = (this->gripper_stepper_rotations >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_rotations);
      union {
        bool real;
        uint8_t base;
      } u_gripper_stepper_enable;
      u_gripper_stepper_enable.real = this->gripper_stepper_enable;
      *(outbuffer + offset + 0) = (u_gripper_stepper_enable.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_enable);
      union {
        bool real;
        uint8_t base;
      } u_electromagnet;
      u_electromagnet.real = this->electromagnet;
      *(outbuffer + offset + 0) = (u_electromagnet.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->electromagnet);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->current_command =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->current_command);
      this->nut_stepper_direction =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_direction);
      this->nut_stepper_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->nut_stepper_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->nut_stepper_angle);
      this->nut_stepper_speed =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_speed);
      this->nut_stepper_rotations =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_rotations);
      union {
        bool real;
        uint8_t base;
      } u_nut_stepper_enable;
      u_nut_stepper_enable.base = 0;
      u_nut_stepper_enable.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->nut_stepper_enable = u_nut_stepper_enable.real;
      offset += sizeof(this->nut_stepper_enable);
      this->gripper_stepper_direction =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_direction);
      this->gripper_stepper_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->gripper_stepper_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->gripper_stepper_angle);
      this->gripper_stepper_speed =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_speed);
      this->gripper_stepper_rotations =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_rotations);
      union {
        bool real;
        uint8_t base;
      } u_gripper_stepper_enable;
      u_gripper_stepper_enable.base = 0;
      u_gripper_stepper_enable.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->gripper_stepper_enable = u_gripper_stepper_enable.real;
      offset += sizeof(this->gripper_stepper_enable);
      union {
        bool real;
        uint8_t base;
      } u_electromagnet;
      u_electromagnet.base = 0;
      u_electromagnet.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->electromagnet = u_electromagnet.real;
      offset += sizeof(this->electromagnet);
     return offset;
    }

    const char * getType(){ return "gripper_control/ArduinoCommand"; };
    const char * getMD5(){ return "096eba23a57eb95a7c57a28fa773c4c6"; };

  };

}
#endif