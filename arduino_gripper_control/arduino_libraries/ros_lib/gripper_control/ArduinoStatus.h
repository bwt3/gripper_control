#ifndef _ROS_gripper_control_ArduinoStatus_h
#define _ROS_gripper_control_ArduinoStatus_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace gripper_control
{

  class ArduinoStatus : public ros::Msg
  {
    public:
      uint8_t current_command;
      uint8_t nut_stepper_direction;
      uint16_t nut_stepper_target_angle;
      uint16_t nut_stepper_current_angle;
      uint8_t nut_stepper_speed;
      uint8_t nut_stepper_number_of_rotations_to_go;
      bool nut_stepper_enabled;
      uint8_t gripper_stepper_direction;
      uint16_t gripper_stepper_target_angle;
      uint16_t gripper_stepper_current_angle;
      uint8_t gripper_stepper_speed;
      uint8_t gripper_stepper_number_of_rotations_to_go;
      bool gripper_stepper_enabled;
      bool electromagnet;

    ArduinoStatus():
      current_command(0),
      nut_stepper_direction(0),
      nut_stepper_target_angle(0),
      nut_stepper_current_angle(0),
      nut_stepper_speed(0),
      nut_stepper_number_of_rotations_to_go(0),
      nut_stepper_enabled(0),
      gripper_stepper_direction(0),
      gripper_stepper_target_angle(0),
      gripper_stepper_current_angle(0),
      gripper_stepper_speed(0),
      gripper_stepper_number_of_rotations_to_go(0),
      gripper_stepper_enabled(0),
      electromagnet(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->current_command >> (8 * 0)) & 0xFF;
      offset += sizeof(this->current_command);
      *(outbuffer + offset + 0) = (this->nut_stepper_direction >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_direction);
      *(outbuffer + offset + 0) = (this->nut_stepper_target_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->nut_stepper_target_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->nut_stepper_target_angle);
      *(outbuffer + offset + 0) = (this->nut_stepper_current_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->nut_stepper_current_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->nut_stepper_current_angle);
      *(outbuffer + offset + 0) = (this->nut_stepper_speed >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_speed);
      *(outbuffer + offset + 0) = (this->nut_stepper_number_of_rotations_to_go >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_number_of_rotations_to_go);
      union {
        bool real;
        uint8_t base;
      } u_nut_stepper_enabled;
      u_nut_stepper_enabled.real = this->nut_stepper_enabled;
      *(outbuffer + offset + 0) = (u_nut_stepper_enabled.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->nut_stepper_enabled);
      *(outbuffer + offset + 0) = (this->gripper_stepper_direction >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_direction);
      *(outbuffer + offset + 0) = (this->gripper_stepper_target_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->gripper_stepper_target_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->gripper_stepper_target_angle);
      *(outbuffer + offset + 0) = (this->gripper_stepper_current_angle >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->gripper_stepper_current_angle >> (8 * 1)) & 0xFF;
      offset += sizeof(this->gripper_stepper_current_angle);
      *(outbuffer + offset + 0) = (this->gripper_stepper_speed >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_speed);
      *(outbuffer + offset + 0) = (this->gripper_stepper_number_of_rotations_to_go >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_number_of_rotations_to_go);
      union {
        bool real;
        uint8_t base;
      } u_gripper_stepper_enabled;
      u_gripper_stepper_enabled.real = this->gripper_stepper_enabled;
      *(outbuffer + offset + 0) = (u_gripper_stepper_enabled.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->gripper_stepper_enabled);
      union {
        bool real;
        uint8_t base;
      } u_electromagnet;
      u_electromagnet.real = this->electromagnet;
      *(outbuffer + offset + 0) = (u_electromagnet.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->electromagnet);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->current_command =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->current_command);
      this->nut_stepper_direction =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_direction);
      this->nut_stepper_target_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->nut_stepper_target_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->nut_stepper_target_angle);
      this->nut_stepper_current_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->nut_stepper_current_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->nut_stepper_current_angle);
      this->nut_stepper_speed =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_speed);
      this->nut_stepper_number_of_rotations_to_go =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->nut_stepper_number_of_rotations_to_go);
      union {
        bool real;
        uint8_t base;
      } u_nut_stepper_enabled;
      u_nut_stepper_enabled.base = 0;
      u_nut_stepper_enabled.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->nut_stepper_enabled = u_nut_stepper_enabled.real;
      offset += sizeof(this->nut_stepper_enabled);
      this->gripper_stepper_direction =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_direction);
      this->gripper_stepper_target_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->gripper_stepper_target_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->gripper_stepper_target_angle);
      this->gripper_stepper_current_angle =  ((uint16_t) (*(inbuffer + offset)));
      this->gripper_stepper_current_angle |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->gripper_stepper_current_angle);
      this->gripper_stepper_speed =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_speed);
      this->gripper_stepper_number_of_rotations_to_go =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->gripper_stepper_number_of_rotations_to_go);
      union {
        bool real;
        uint8_t base;
      } u_gripper_stepper_enabled;
      u_gripper_stepper_enabled.base = 0;
      u_gripper_stepper_enabled.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->gripper_stepper_enabled = u_gripper_stepper_enabled.real;
      offset += sizeof(this->gripper_stepper_enabled);
      union {
        bool real;
        uint8_t base;
      } u_electromagnet;
      u_electromagnet.base = 0;
      u_electromagnet.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->electromagnet = u_electromagnet.real;
      offset += sizeof(this->electromagnet);
     return offset;
    }

    const char * getType(){ return "gripper_control/ArduinoStatus"; };
    const char * getMD5(){ return "ebef11153a4d15f7fdaa5673c415967c"; };

  };

}
#endif