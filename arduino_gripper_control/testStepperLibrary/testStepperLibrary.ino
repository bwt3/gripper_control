#include "A4988.h"

A4988 stepperMotors = A4988();

bool looped = false;
long previousTime = 0;
bool isClockwise = true;

void setup()
{
  stepperMotors.init();
  Serial.begin(250000);
}

void loop()
{
  long currentTime = millis;
  if (looped == false)
  {
    delay(5000);
    stepperMotors.setSpeed(STEPPER_2,255);
    stepperMotors.setTargetAngle(STEPPER_2, 0, 100);
    stepperMotors.changeDirection(STEPPER_2, ANTICLOCKWISE_STEPPER_DIR);
    stepperMotors.setSpeed(STEPPER_1,255);
    stepperMotors.setTargetAngle(STEPPER_1, 0, 100);
    stepperMotors.changeDirection(STEPPER_1, ANTICLOCKWISE_STEPPER_DIR);
    looped = true;
  }
  stepperMotors.stepperMotorLoop();

  if (currentTime >= (previousTime + 2000))
  {
    Serial.print("CurrentTime: ");
    Serial.println(currentTime, DEC);
    if (isClockwise == false)
    {
      stepperMotors.setSpeed(STEPPER_2, 120);
      stepperMotors.changeDirection(STEPPER_2, CLOCKWISE_STEPPER_DIR);
      isClockwise = true;

    }
    else
    {
      stepperMotors.setSpeed(STEPPER_2, 60);
      stepperMotors.changeDirection(STEPPER_2, ANTICLOCKWISE_STEPPER_DIR);
      isClockwise = false;
    }

    previousTime = millis();
  }

}