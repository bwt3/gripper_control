//Code similar to non auto version but includes Clare's subscribers

#include "ros/ros.h"
#include <iiwa_ros.h>
#include <tf/transform_listener.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/PointCloud.h"
#include "std_msgs/Float32.h"
#include <cmath>
#include "ArduinoCommandDef.h"


//Library for the End Effector Grippers
ros::NodeHandle * nh;
#include "abstractGripper.h"

//GLOBAL DEFINE VARIABLES

//END EFFECTOR PARAMETERS
#define END_EFFECTOR_GRIPPER_LENGTH         0.20485
#define END_EFFECTOR_SOCKET_LENGTH_AT_TIP   0.15780
#define END_EFFECTOR_SOCKET_LENGTH_INTERNAL 0.08700
#define END_EFFECTOR_SOCKET_LENGTH          END_EFFECTOR_SOCKET_LENGTH_AT_TIP - END_EFFECTOR_SOCKET_LENGTH_INTERNAL


//Diameter of the wheel
#define WHEEL_DIAMETER_M                    0.262
#define WHEEL_BLUE_PART_M                   0.030
#define NUT_LENGTH                          0.025
#define SOCKET_REMOVAL_ROTATIONS            7

#define PI2_Value                           6.283185                                 

//Movement Speeds
#define FASTER_MOVEMENT_SPEED   0.75
#define MOVING_TO_WHEEL_SPEED   0.5
#define TOUCH_ON_SPEED 0.25

//MECHANICAL ZERO POSITION;
#define MECH_ZERO_POSITION_X        0.0
#define MECH_ZERO_POSITION_Y        0.0
#define MECH_ZERO_POSITION_Z        1.31
#define MECH_ZERO_POSITION_ROLL     0.0
#define MECH_ZERO_POSITION_PITCH    0.0
#define MECH_ZERO_POSITION_YAW      0.0

//START POSITION TO REMOVE WHEEL AT 
#define START_POSITION_X     0.55
#define START_POSITION_Y     0.010
#define START_POSITION_Z     0.20342
#define START_POSITION_ROLL  0.0
#define START_POSITION_PITCH 1.57079632679 //I.e. this is 90 degrees in radians.
#define START_POSITION_YAW   0.0

//Touch On POSITION TO REMOVE WHEEL
#define TOUCH_ON_POSITION_X     0.934850
#define TOUCH_ON_POSITION_Y     0.010
#define TOUCH_ON_POSITION_Z     0.20342
#define TOUCH_ON_POSITION_ROLL  0.0
#define TOUCH_ON_POSITION_PITCH 1.57079632679
#define TOUCH_ON_POSITION_YAW   0.0

//Drop Location
#define DROP_LOCATION_X         0.6
#define DROP_LOCATION_Y         0.2
#define DROP_LOCATION_Z         0.3
#define DROP_LOCATION_ROLL      0.0
#define DROP_LOCATION_PITCH     1.57079632679
#define DROP_LOCATION_YAW       0.0

//iiwa Robot Subscriber and Publisher 
ros::Publisher   ros_position_pub; //iiwa Position Publisher
ros::Subscriber  ros_position_sub; //iiwa Position Subscriber
iiwa_ros::iiwaRos iiwa_robot;

int wheelDiameterMM = 0;
int visionWheelDiameterMM = 0;

//TODO Clare Subscriber Declaration
ros::Subscriber image_position;
ros::Subscriber image_diameter;

geometry_msgs::Pose imagePose;

//Variables to do with Vision
bool validImagePoint = false;
bool validImagePointWhile = false;
bool validDiameter = false;

geometry_msgs::Quaternion convertRollPitchYawQuaternion(double roll, double pitch, double yaw);

//Required call backs and variable containing the current position of the robot
//fed back by the pos_subscriber. Ran using an Asynchronous Spinner with 1 thread
geometry_msgs::PoseStamped current_position;
void updatePosition(const geometry_msgs::PoseStamped::ConstPtr& currentPosition)
{
    current_position.header = currentPosition->header;
    current_position.pose.position.x = currentPosition->pose.position.x;
    current_position.pose.position.y = currentPosition->pose.position.y;
    current_position.pose.position.z = currentPosition->pose.position.z;
    current_position.pose.orientation.x = currentPosition->pose.orientation.x;
    current_position.pose.orientation.y = currentPosition->pose.orientation.y;
    current_position.pose.orientation.z = currentPosition->pose.orientation.z;
    current_position.pose.orientation.w = currentPosition->pose.orientation.w;
}

void updateImagePoint(const sensor_msgs::PointCloud::ConstPtr& currentImagePosition)
{
    //TODO Calclulate Offsets.
    if (validImagePoint == false)
    {
        imagePose.position.x = currentImagePosition->points[0].x;
        imagePose.position.y = currentImagePosition->points[0].y;
        imagePose.position.z = currentImagePosition->points[0].z;

        imagePose.orientation = convertRollPitchYawQuaternion(TOUCH_ON_POSITION_ROLL, TOUCH_ON_POSITION_PITCH, TOUCH_ON_POSITION_YAW);
	validImagePointWhile = true;
    }
}

//Value comes in as mm.
void updateDiameter(const std_msgs::Float32::ConstPtr& wheelDiameter)
{
    if (validDiameter == false)
    {
        visionWheelDiameterMM = (int) (wheelDiameter->data * 1000.0);
        validDiameter = true;
    }
}

//printPosition prints the position in x,y,z and roll pitch and yaw.
void printPosition(geometry_msgs::PoseStamped position)
{
    ROS_INFO("X: %f", position.pose.position.x);
    ROS_INFO("Y: %f", position.pose.position.y);
    ROS_INFO("Z: %f", position.pose.position.z);
    
    //ROS_INFO("Angle in Quaternion\n");

    //ROS_INFO("X: %f", position.pose.orientation.x);
    //ROS_INFO("Y: %f", position.pose.orientation.y);
    //ROS_INFO("Z: %f", position.pose.orientation.z);
    //ROS_INFO("W: %f", position.pose.orientation.w);
    
    //Using TF to convert between the Quarterion and Roll Pitch and Yaw
    tf::Quaternion currentTFQ;
    tf::quaternionMsgToTF(position.pose.orientation, currentTFQ);
    tfScalar rollTF;
    tfScalar pitchTF;
    tfScalar yawTF;
    tf::Matrix3x3 convertedAngular(currentTFQ);
    convertedAngular.getRPY(rollTF, pitchTF, yawTF, 1); //Gets the roll pitch and yaw

    double rollDouble, pitchDouble, yawDouble;

    rollDouble  = (double) rollTF;
    pitchDouble = (double) pitchTF;
    yawDouble   = (double) yawTF;

     ROS_INFO("Angular position: \n");
     ROS_INFO("Roll %f", rollDouble);
     ROS_INFO("Pitch %f", pitchDouble);
     ROS_INFO("Yaw %f", yawDouble); 

}

//getPositionToMoveFromUser Gets a new position to move to from the user.
geometry_msgs::PoseStamped getPositionToMoveFromUser()
{

  geometry_msgs::PoseStamped tempPose;
  tf::Quaternion currentTFQ, q;
  geometry_msgs::Quaternion geo_q;
  float x,y,z,roll,pitch,yaw;
  
  printf("Please provide the position to move to:");
  printf("X:");
  scanf("%f", &x);

  printf("Y:");
  scanf("%f", &y);

  printf("Z:");
  scanf("%f", &z);

  printf("Please provide the roll, pitch and yaw for the end effector in radians\n");
  printf("Roll");
  scanf("%f", &roll);

  printf("Pitch");
  scanf("%f", &pitch);

  printf("Yaw");
  scanf("%f", &yaw);

  printf("The values read in were x:%f, y:%f, z:%f, roll:%f, pitch:%f, yaw:%f", x,y,z,roll,pitch,yaw);

  //Create Quaternion from the roll pitch and yaw
  q = tf::createQuaternionFromRPY(roll, pitch, yaw);
  q.normalize();
  tf::quaternionTFToMsg(q,geo_q);

  tempPose.pose.position.x = x;
  tempPose.pose.position.y = y;
  tempPose.pose.position.z = z;
  
  tempPose.pose.orientation = geo_q;

  return tempPose;
}

//convertRollPitchYawQuaternion converts roll pitch and yaw to geometry msgs quarternion
geometry_msgs::Quaternion convertRollPitchYawQuaternion(double roll, double pitch, double yaw)
{
    tf::Quaternion tf_quarternion;
    geometry_msgs::Quaternion geo_quarternion;
    //
    tf_quarternion = tf::createQuaternionFromRPY(roll,pitch,yaw);
    tf_quarternion.normalize(); //Normalise the quaternion to ensure that the value is valid
    tf::quaternionTFToMsg(tf_quarternion,geo_quarternion);
    
    return geo_quarternion;
}

//moveRobotCommand function takes in position to move to and the user check and publises the
//new posution to iiwa. Blocks until iiwa has completed the task in hand
bool moveRobotCommand(geometry_msgs::PoseStamped positionToMoveTo, bool askUserToCheck)
{
  bool check = askUserToCheck;
  if (askUserToCheck == true)
  {
    ROS_INFO("Are you sure you want to move to position\n");
    printPosition(positionToMoveTo);
    ROS_INFO("Y/n");
char result;
    scanf("%c", &result);
    printf("%c", result);
    /*
    if ((result == 'Y') || (result == 'y'))
    {
        check = false;
    }
    else
    {
        ROS_INFO("Movement Aborted");
        return true;
    }
    */
  }

  //Publish the position to KUKA
  ros_position_pub.publish(positionToMoveTo);
  ROS_INFO("Published new position to KUKA");
  ros::spinOnce();

  ros::Duration(1.0).sleep();
  

  //Get the time to move the new position and block until then
  double timeToDes =  iiwa_robot.getTimeToDestinationService().getTimeToDestination();
  ROS_INFO("The time to complete the movement is %f\n", timeToDes);

  //Block inside loop until the robot confirms it has reached it's position
  while ((timeToDes > 0 ) && (timeToDes != -999))
  {
     ros::Duration(0.05).sleep();
     timeToDes = iiwa_robot.getTimeToDestinationService().getTimeToDestination();
     ros::spinOnce();
  }

  //If there was an error. Return false
  if (timeToDes == -999)
  {
      ROS_INFO("Time returned from iiwa was not valid. Command failed");
      ROS_ERROR_STREAM("Time returned from iiwa was not valid. Command failed"); 
      return false;
  }
  else
  {
      ROS_INFO("iiwa moved to point successfuly.\nThe current position is");
      ros::spinOnce();
      printPosition(current_position);
      return true;
  }
}


//moveToMechanicalZero sets the position to move the robot to and then 
bool moveToMechanicalZero()
{
    geometry_msgs::PoseStamped zero_position;
    zero_position.pose.position.x  = MECH_ZERO_POSITION_X;
    zero_position.pose.position.y  = MECH_ZERO_POSITION_Y;
    zero_position.pose.position.z  = MECH_ZERO_POSITION_Z;

    zero_position.pose.orientation = convertRollPitchYawQuaternion(MECH_ZERO_POSITION_ROLL, MECH_ZERO_POSITION_PITCH, MECH_ZERO_POSITION_YAW);
    
    //Set to 3/4 Speed
    //iiwa_robot.getPathParametersService().setPathParameters(FASTER_MOVEMENT_SPEED,FASTER_MOVEMENT_SPEED);

    ROS_INFO("About to Move to Mechanical Zero\n");
    bool result = moveRobotCommand(zero_position, true);
    return result;
}


bool moveTolineUpOnWheelStartPos()
{
    //TODO Check whether this position is too close to robot based on vision.
    ROS_INFO("Starting to move inline to wheel routine");
    geometry_msgs::PoseStamped start_pos;
    start_pos.pose.position.x  = START_POSITION_X;
    start_pos.pose.position.y  = imagePose.position.y;
    start_pos.pose.position.z  = imagePose.position.z;

    start_pos.pose.orientation = convertRollPitchYawQuaternion(START_POSITION_ROLL, START_POSITION_PITCH, START_POSITION_YAW);

    ROS_INFO("Getting Parameters");
    //Set to 3/4 Speed
    //iiwa_robot.getPathParametersService().setJointRelativeVelocity(0.5);
    //iiwa_robot.getPathParametersService().setJointRelativeAcceleration(0.1);

    ROS_INFO("About to Move to inline wheel.\n");
    bool result = moveRobotCommand(start_pos,true);

    return result;
}

bool moveBeforeTouchOnPosition()
{
    
    MoveGripperDistanceBetweenMM(300);

    //TODO Calculate Touch on Position
    geometry_msgs::PoseStamped touchon_pos;
    touchon_pos.pose.position.x = imagePose.position.x - END_EFFECTOR_GRIPPER_LENGTH; //6cms off
    touchon_pos.pose.position.y = imagePose.position.y;
    touchon_pos.pose.position.z = imagePose.position.z;
    
    if (touchon_pos.pose.position.x >= 0.7524)
    {
	    ROS_INFO("Position was crap");
	    return false;
    }

    touchon_pos.pose.orientation = convertRollPitchYawQuaternion(START_POSITION_ROLL, START_POSITION_PITCH, START_POSITION_YAW);

    //iiwa_robot.getPathParametersService().setPathParameters(MOVING_TO_WHEEL_SPEED,MOVING_TO_WHEEL_SPEED);

    ROS_INFO("About to Move Onto the Wheel.\n");
    bool result = moveRobotCommand(touchon_pos,true);
    return result;
}


bool askUserWhetherLinedUp()
{
    bool linedUp = false;
    char userChoice;
    float userInput;
    bool result;
    geometry_msgs::PoseStamped new_position;

    while (!linedUp)
    {
        new_position = current_position;

        ROS_INFO("Is the socket lined up on the nut Y/n  (A to abort)\n");
        scanf("%c", &userChoice);

        if (userChoice == 'Y' || userChoice == 'y')
        {   
            linedUp = true;
            break;
        }
        else if (userChoice == 'A' || userChoice == 'a')
        {
            ROS_INFO("Exiting Program\n");
            return false;
        }

        ROS_INFO("Does the socket need to the left or right (In robot's prespective)?. Type distance in mm. (+ Left)");
        scanf("%f", &userInput);

        userInput = userInput / 1000.0;
        
        if (userInput != 0.0)
        {
            new_position.pose.position.y = new_position.pose.position.y + userInput;
        }

        ROS_INFO("Does the socket need to higher or lower?. Type distance in mm. (+ upwards)");
        scanf("%f", &userInput);

        userInput = userInput / 1000.0;
        
        if (userInput != 0.0)
        {
            new_position.pose.position.z = new_position.pose.position.z + userInput;
        }

        //iiwa_robot.getPathParametersService().setPathParameters(TOUCH_ON_SPEED,TOUCH_ON_SPEED);

        ROS_INFO("About to Move to new position");
        result = moveRobotCommand(new_position,true);
    }

    return result;
}


bool moveInToWheel(double wheelDiameterM)
{
    bool result = false;
    char userInput;

    //Check that grippers are wide enough
    wheelDiameterMM = int (wheelDiameterM * 1000.0);
    
    ////Moves the grippers wider if they are not far enough apart.
    //if (getGripperCurrentDiameter() + 10 >= wheelDiameterMM)
    //{
    //    if (!MoveGripperDistanceBetweenMM(wheelDiameterMM + 10))
    //    {
    //        ROS_ERROR("Problem with Arduino opening to the correct diameter");
     //       return false;
     //   }
    //}
  

    //Disable Socket Stepper Motor
    NutEnable(false);
    geometry_msgs::PoseStamped end_position;
        ROS_INFO("About to Move Socket Fully Over\n");
    for (int i = 0; i < 10; i++)
    {
        end_position.pose.position.x = current_position.pose.position.x + ((NUT_LENGTH + (END_EFFECTOR_GRIPPER_LENGTH - END_EFFECTOR_SOCKET_LENGTH_AT_TIP)) / 10.0);
        end_position.pose.position.y = current_position.pose.position.y;
        end_position.pose.position.z = current_position.pose.position.z;

        end_position.pose.orientation = current_position.pose.orientation;

        //iiwa_robot.getPathParametersService().setPathParameters(TOUCH_ON_SPEED,TOUCH_ON_SPEED);
        
        result = moveRobotCommand(end_position, false);
    }
    //Move up the z axis a little to ensure perfect turning

    //end_position.pose.position = current_position.pose.position;
    //end_position.pose.orientation = current_position.pose.orientation;
    //end_position.pose.position.z = current_position.pose.position.z + 0.002;
    //result = moveRobotCommand(end_position, false);
    
    bool onNut = false;

    ROS_INFO("Is the socket to the blue position? Y/n");
    scanf("%c", &userInput);
    if (userInput == 'Y' || userInput == 'y')
    {
        onNut = true;
    }
    float distance;
    while (!onNut)
    {
        ROS_INFO("How far to move x axis (In robot's prespective)?. Type distance in mm. (+ Towards Wheel)");
        scanf("%f", &distance);

        distance = distance / 1000.0;
        
        if (distance != 0.0)
        {
            end_position.pose.position.x = end_position.pose.position.x + distance;
        }

        ROS_INFO("About to Move Socket Fully Over\n");
        result = moveRobotCommand(end_position, true);

        ROS_INFO("Is the socket to the blue position? Y/n");
        scanf("%c", &userInput);
        
        if (userInput == 'Y' || userInput == 'y')
        {
        onNut = true;
        }
    }

}
bool removeNut()
{
    char userInput;
    ROS_INFO("Spinning Socket Anticlockwise\n");
    NutEnable(true);
    MoveSocketMultipleRotations(0.0,CLOCKWISE_STEPPER_DIR, SOCKET_REMOVAL_ROTATIONS); 

    ROS_INFO("Is the nut off the thread\n");
    scanf("%c", &userInput);
    bool nutOffThread = false;
    if (userInput == 'Y' || userInput == 'y')
    {
        nutOffThread = true;
    }

    while (!nutOffThread)
    {
        ROS_INFO("Spin One Rotation");
        MoveSocketMultipleRotations(0.0,CLOCKWISE_STEPPER_DIR, 1);

        ROS_INFO("Is the nut off the socket\n");
        scanf("%c", &userInput); 

        if (userInput == 'Y' || userInput == 'y')
        {
            nutOffThread = true;
        }
    }
    return true;
}


bool moveOffHub()
{
    bool result = false;
    char userInput;

    geometry_msgs::PoseStamped end_position;
    
    //Move socket to the back of the nut and move grippers in after the socket 
    for (int i = 0; i < 5 ; i++)
    {
        end_position.pose.position.x = current_position.pose.position.x + (WHEEL_BLUE_PART_M / 4.0);
        end_position.pose.position.y = current_position.pose.position.y;
        end_position.pose.position.z = current_position.pose.position.z + (0.003/4.0);

        end_position.pose.orientation = current_position.pose.orientation;

        //iiwa_robot.getPathParametersService().setPathParameters(TOUCH_ON_SPEED,TOUCH_ON_SPEED);
        
        result = moveRobotCommand(end_position, false);
    }


    //Move Grippers in to ensure wheel will come off
    bool grippersGripping = false;

    ROS_INFO("Moving grippers in ");
     //Move grippers in.
    MoveGripperDistanceBetweenMM(268); //TODO remove 5mm if this proves to not allow enough control.

    ROS_INFO("Have grippers taken hold of wheel Y/n  A to abort");
    scanf("%c", &userInput);

    if (userInput == 'Y' || userInput == 'y')
    {
        grippersGripping = true;
    }
    
    int distanceInt = wheelDiameterMM;
    int userDistance = 0;
    
    while (!grippersGripping)
    {
        ROS_INFO("How far to move the grippers in?. Type distance in mm. (+ Towards Wheel)");
        scanf("%d", &userDistance);

        distanceInt = distanceInt - userDistance;
        
        if (distanceInt != 0.0)
        {
            MoveGripperDistanceBetweenMM(distanceInt);
        }

        ROS_INFO("Have grippers taken hold of wheel Y/n ");
        scanf("%c", &userInput);

        if (userInput == 'Y' || userInput == 'y')
        {
            grippersGripping = true;
        }
    }
    

    //Starting Moveing off Hub#
    ROS_INFO("Move off Hub");
    bool farEnoughAway = false;

    geometry_msgs::PoseStamped offHub_position;

    double distancePerMovement = (current_position.pose.position.x - START_POSITION_X) / 20.0;

    for (int i = 0; i < 20; i++)
    {
        offHub_position.pose.position.x = current_position.pose.position.x - distancePerMovement;
        offHub_position.pose.position.y = current_position.pose.position.y;
        offHub_position.pose.position.z = current_position.pose.position.z;
        offHub_position.pose.orientation = current_position.pose.orientation;        

        ROS_INFO("Set Speed");
        //iiwa_robot.getPathParametersService().setPathParameters(TOUCH_ON_SPEED,TOUCH_ON_SPEED);

        ROS_INFO("About to move the wheel off the hub\n");
        result = moveRobotCommand(offHub_position, false);

    }

    ROS_INFO("Is the Wheel away at least 15cm away from end of bolt Y/n");
    scanf("%c", &userInput);

    if (userInput == 'Y' || userInput == 'y')
    {
        farEnoughAway = true;
    }

    while (!farEnoughAway)
    {
        ROS_INFO("How far to move iiwa back ?. Type distance in mm. (+ Towards iiwa base)");
        scanf("%d", &userDistance);

        float userDistanceFloat = userDistance / 1000.0;

        offHub_position.pose.position.x = current_position.pose.position.x - userDistanceFloat;
        offHub_position.pose.position.y = current_position.pose.position.y;
        offHub_position.pose.position.z = current_position.pose.position.z;
        offHub_position.pose.orientation = current_position.pose.orientation;   
    

        ROS_INFO("Is the Wheel away at least 15cm away from end of bolt  ");
        scanf("%c", &userInput);

        if (userInput == 'Y' || userInput == 'y')
        {
            farEnoughAway = true;
        }
    }


    return result;
}

bool dropWheel()
{
    bool result = false;
    geometry_msgs::PoseStamped dropWheel_position;

    //Move to the drop location
    dropWheel_position.pose.position.x = DROP_LOCATION_X;
    dropWheel_position.pose.position.y = DROP_LOCATION_Y;
    dropWheel_position.pose.position.z = DROP_LOCATION_Z;

    dropWheel_position.pose.orientation = convertRollPitchYawQuaternion(DROP_LOCATION_X, DROP_LOCATION_Y, DROP_LOCATION_Z);

    //iiwa_robot.getPathParametersService().setPathParameters(TOUCH_ON_SPEED, TOUCH_ON_SPEED);

    ROS_INFO("About to move to the drop position");
    result = moveRobotCommand(dropWheel_position, true);

    //Then turn the end effector yaw position.

    dropWheel_position.pose.position = current_position.pose.position;
    dropWheel_position.pose.orientation = convertRollPitchYawQuaternion(START_POSITION_ROLL, PI2_Value/2, START_POSITION_YAW);

    ROS_INFO("About to turn the end effector towards the ground\n");
    result = moveRobotCommand(dropWheel_position, true);

    dropWheel_position.pose.position.x = DROP_LOCATION_X;
    dropWheel_position.pose.position.y = DROP_LOCATION_Y;
    dropWheel_position.pose.position.z = END_EFFECTOR_GRIPPER_LENGTH;

    dropWheel_position.pose.orientation = current_position.pose.orientation;

    ROS_INFO("About to move the end effector towards the ground");
    result = moveRobotCommand(dropWheel_position, true);

    ROS_INFO("Moving Grippers Apart");
    //Then release grippers
    MoveGripperDistanceBetweenMM(wheelDiameterMM + 15);

    // Move End Effector upwards by 10mm
    dropWheel_position.pose.position.x = DROP_LOCATION_X;
    dropWheel_position.pose.position.y = DROP_LOCATION_Y;
    dropWheel_position.pose.position.z = END_EFFECTOR_GRIPPER_LENGTH + 0.15;

    dropWheel_position.pose.orientation = current_position.pose.orientation;

    ROS_INFO("Move End Effector Upwards away from wheel.");
    result = moveRobotCommand(dropWheel_position, true);

    return result;
}

bool moveOff()
{

}

bool removeWheel()
{
    ROS_INFO("Starting the Wheel Removal Process\n");
    char userChoice;
    //Process
    //Move to line up on wheel.
    //Then move grippers to wider than the diameter of the wheel.
    //Then move the end effector so it's just off the end effector of the wheel.
    //Ask the user whether the robot is lined up.
    //Then move socket in.
    //Move grippers into diameter of the wheel
    //Then spin nut.
    //Move robot away from wheel.
    //Then place wheel down. 
    moveTolineUpOnWheelStartPos();
    moveBeforeTouchOnPosition();
    askUserWhetherLinedUp();
    moveInToWheel(WHEEL_DIAMETER_M);
    removeNut();
    moveOffHub();

    
    //Do you want to place the wheel down
    //ROS_INFO("Do you want to place wheel down");
    scanf("%c", &userChoice);
    if (userChoice == 'Y' || userChoice == 'y')
    {
        dropWheel();
        moveToMechanicalZero();
    }

}

bool installWheel()
{

}


// Main Program
int main(int argc, char ** argv)
{
    //ROS SETUP
    ros::init(argc, argv, "non_auto_plane");
    ros::NodeHandle n;
    nh = &n;
    ros::WallRate loop_rate(500);
    
	ros::AsyncSpinner spinner(1);
    spinner.start();

    char delay;

    ROS_INFO("Setup ROS Correctly");
    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);


    //initialise the iiwa robot library
    iiwa_robot.init();
    initGripper();

    //Subscribe and Publish the Robot Position. These are backup since the robot's version is crap.
    ros_position_sub = n.subscribe("/iiwa/state/CartesianPose",1, updatePosition);
    ros_position_pub = n.advertise<geometry_msgs::PoseStamped>("iiwa/command/CartesianPose",1);

    ROS_INFO("Init iiwa Robot");
    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);

    image_position = n.subscribe("/tf_point", 1, updateImagePoint);
    image_diameter = n.subscribe("/width", 1, updateDiameter);


    //Checking whether there is a valid prose from the robot to check whether the connection is working correctly.
    if (iiwa_robot.getCartesianPose(current_position))    
    {
        ROS_INFO("The current position of the iiwa robot is:\n");
        printPosition(current_position);
    }
    else
    {
        ROS_INFO("No Position Recevied by KUKA Program exiting");
        return 1;
    }


    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);

    while ((validImagePointWhile == false))
    {
        if (validImagePoint == true)
        {
            ROS_INFO("Received Valid Centre Point");
        }
        if (validDiameter == true)
        {
            ROS_INFO("Received Valid Diameter\n");
        }
        //SpinOnce to ensure we have a valid point
        ros::spinOnce();

        ros::Duration(0.1).sleep();
    }
    ROS_INFO("SLEEPING 10s ......");
    ros::Duration(10).sleep();
    validImagePoint = true;

    ROS_INFO("The vision point detected is x: %f, y: %f z: %f", imagePose.position.x, imagePose.position.y, imagePose.position.z);
    

    ROS_INFO("Continuing to remove wheel");
    removeWheel();
    
    return 0;
}
