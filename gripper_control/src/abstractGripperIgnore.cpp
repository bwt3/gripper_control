#include "abstractGripper.h"

//Global ROS Arduino Publishers and Subscribers

//ROS Async Spinner

//CommandComplete
bool commandCompleted = true;
int  nutCurrentAngle = 0;
int  gripperCurrentDistance = 200;

bool gripperEnabled = true;
bool nutEnabled     = true;


AbstractGripper::AbstractGripper(ros::NodeHandle * nodehandle): nh_(*nodehandle)
{
  ros_arduino_sub = nh_.subscribe("/testLibrary/PC_Arduino_TX", 1, &AbstractGripper::receivedArduinoStatus, this); // Buffersize is one 
  ros_arduino_pub = nh_.advertise<gripper_control::ArduinoCommand>("/testLibrary/PC_Arduino_RX",20);

        gripper_control::ArduinoCommand msg;
      msg.current_command           = ELECTROMAGNET_COMMAND;
      msg.electromagnet    = true;

      printf("Current Command %d\n", msg.current_command);
      ros_arduino_pub.publish(msg);
}

    //receivedArduinoStatus function is a call back which states whether a command has completed.
    void  AbstractGripper::receivedArduinoStatus(const gripper_control::ArduinoStatus::ConstPtr &msg)
    {
      if (msg->current_command == WAIT_COMMAND)
      {
        commandCompleted = true;
      }

        printf("Current Command: %d\n",msg->current_command);

        //printf("Nut Stepper Direction: %d\n", msg->nut_stepper_direction);
        //printf("Nut Stepper Target Angle: %d\n", msg->nut_stepper_target_angle);
        //printf("Nut Stepper Current Angle %d\n", msg->nut_stepper_current_angle);
        //printf("Nut Stepper Speed: %d\n", msg->nut_stepper_speed);
        //rintf("Nut Stepper Rotations: %d\n", msg->nut_stepper_number_of_rotations_to_go);

        //printf("Gripper Stepper Direction %d\n", msg->gripper_stepper_direction);
        //printf("Gripper Stepper Target Angle %d\n", msg->gripper_stepper_target_angle);
        //printf("Gripper Stepper Current Angle %d\n", msg->gripper_stepper_current_angle);
        //printf("Gripper Stepper Speed %d\n", msg->gripper_stepper_speed);
        //printf("Gripper Stepper Rotations still to complete %d\n", msg->gripper_stepper_number_of_rotations_to_go);
    
        nutCurrentAngle = msg->nut_stepper_current_angle;
        gripperEnabled  = msg->gripper_stepper_enabled;
        nutEnabled      = msg->nut_stepper_enabled;
    }


    //function which sets up gripper publisher and launches the 
    void AbstractGripper::init()
    {
      printf("Hello init\n");

      printf("Hello\n");
      //Setup ROSSerial Publishers and Subscribers
      


    }

    //Move Sotcket Position
    bool AbstractGripper::MoveSocketPosition(float angle, int direction)
    {
      int angleInt = angle * 10;
      int noOfSteps = angle / 18;
      angleInt = noOfSteps * 18;
      
      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = 0;
      msg.gripper_stepper_angle     = angleInt;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;
      msg.gripper_stepper_enable    = true;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((!commandCompleted) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    bool AbstractGripper::MoveSocketMultipleRotations(float angle, int direction, int rotations)
    {
      /*
      int angleInt = angle * 10;
      int noOfSteps = angle / 18;
      angleInt = noOfSteps * 18;
      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command       = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.nut_stepper_rotations = rotations;
      msg.nut_stepper_angle     = angleInt;
      msg.nut_stepper_direction = direction;
      msg.nut_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;
      msg.nut_stepper_enable    = true;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      ros::spinOnce();
      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((!commandCompleted) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        return true;
      }
      */
    }

    bool AbstractGripper::MoveGripperDistanceBetweenMM(int distance)
    {
      printf("Entered Loop\n");
      /*
      int direction;
      //Check whether the distance is within the limits of the device
      if ((distance < INNER_DISTANCE_LIMIT) || (distance > OUTER_DISTANCE_LIMIT))
      {
        ROS_INFO("GRIPPERS: Distance Outwidth the bounds of the device.\n");
        return false;
      }
      int distanceToTravel = gripperCurrentDistance - distance;

      printf("Distance To Travel %d\n", distanceToTravel);

      if (distanceToTravel < 0)
      {
        distanceToTravel = distanceToTravel * -1;
      }

      int numberOfRotations = distanceToTravel / DISTANCE_PER_ROTATION;
      printf("Rotations %d\n", numberOfRotations);
      
      int leftOver   = distanceToTravel % DISTANCE_PER_ROTATION;

      double angle  = (double) (leftOver / (double) DISTANCE_PER_ROTATION ) * 3600.0;
      printf("Angle Float %f", angle);
      int   angleInt = (int) angle;
      
      int closestAngle = angleInt % 18;

      if (closestAngle != 0)
      {
        angleInt = angleInt / 18;
        angleInt = angleInt * 18;
      }

      printf("Angle Int\n %d", angleInt);

      //Determine whether to move clockwise or anti clockwise. If the current distance
      if (gripperCurrentDistance < distance)
      {
        direction = CLOCKWISE_STEPPER_DIR;
      }
      else
      {
        direction = ANTICLOCKWISE_STEPPER_DIR;
      }

      //Construct message
      printf("Rotations %d\n", numberOfRotations);


      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = numberOfRotations;
      msg.gripper_stepper_angle     = angleInt;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = GRIPPER_STEPPER_SPEED_RPM;
      msg.gripper_stepper_enable    = true;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      printf("Published Command\n");
      sleep(5);


      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      
      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        if (direction == CLOCKWISE_STEPPER_DIR)
        {
          gripperCurrentDistance = gripperCurrentDistance + distance;
        }
        else
        {
          gripperCurrentDistance = gripperCurrentDistance - distance;
        }
        return true;

      }
      */
  
    }

    int AbstractGripper::getNutAngle()
    {
      return nutCurrentAngle;
    }

    void AbstractGripper::GripperEnable(bool enabled)
    {
      if ((gripperEnabled && enabled) || (!gripperEnabled && !enabled))
      {
        return;
      }
      else
      {
        gripper_control::ArduinoCommand msg;
        msg.gripper_stepper_enable    = enabled;
      }
    }

    void AbstractGripper::NutEnable(bool enabled)
    {
      if ((nutEnabled && enabled) || (!nutEnabled && !enabled))
      {
        return;
      }
      else
      {
        gripper_control::ArduinoCommand msg;
        msg.gripper_stepper_enable    = enabled;
      }
    }



