//Test for using the KUKA iiwa robot using catiezian space and Gazebo
#include <stdio.h>
#include <stdlib.h>
#include "ros/ros.h"
#include <iiwa_ros.h>
#include "geometry_msgs/PoseStamped.h"

ros::Publisher position_pub;
iiwa_ros::iiwaRos testRobot;


void sleepForMotion(iiwa_ros::iiwaRos& iiwa, const double maxSleepTime) {

  double ttd = iiwa.getTimeToDestinationService().getTimeToDestination();

  ros::Time start_wait = ros::Time::now();

  while (ttd < 0.0 && (ros::Time::now() - start_wait) < ros::Duration(maxSleepTime)) {

    ros::Duration(0.5).sleep();

    ttd = iiwa.getTimeToDestinationService().getTimeToDestination();

  }

  if (ttd > 0.0) {

    ROS_INFO_STREAM("Sleeping for " << ttd << " seconds.");

    ros::Duration(ttd).sleep();

  } 

}


int main(int argc, char ** argv)
{
  ros::init(argc, argv, "testRobot");
  ros::NodeHandle node;

  testRobot.init();


	ros::AsyncSpinner spinner(1);
    spinner.start();


  double ros_rate;

	node.param("ros_rate", ros_rate, 0.1); // 0.1 Hz = 10 seconds

	ros::Rate* loop_rate_ = new ros::Rate(ros_rate);

  geometry_msgs::PoseStamped currentPose;
  bool result;
  //Get the current postion and print this out
    result = testRobot.getCartesianPose(currentPose);
    printf("Received True from the Robot for getting the postiion\n");
    geometry_msgs::Pose tempPose = currentPose.pose;
    printf("Current X %f Y %f Z %f\n", tempPose.position.x, tempPose.position.y, tempPose.position.z);

double positionz = 0.0;
//sleep(5000);
while (ros::ok())
{
  geometry_msgs::Pose newPose;
  geometry_msgs::PoseStamped stampedPose;
  newPose.position.x = 0;
  newPose.position.y = 0;
  newPose.position.z = positionz - 0.1;



stampedPose.pose = newPose;

printf("Started Sent Command\n");

testRobot.setCartesianPose(stampedPose);
printf("Command Sent\n");

geometry_msgs::PoseStamped currentPose;

  //Get the current postion and print this out
    result = testRobot.getCartesianPose(currentPose);
    if (result == true)
    {
      printf("Received True from the Robot for getting the postiion\n");
      geometry_msgs::Pose tempPose = currentPose.pose;
      printf("Current X %f Y %f Z %f\n", tempPose.position.x, tempPose.position.y, tempPose.position.z);
    }
    else
    {
      printf("Couldn't receive valid position\n");
    }

loop_rate_->sleep();

}

}
