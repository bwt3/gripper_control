#include "ros/ros.h"
#include <iiwa_ros.h>
#include <moveit/move_group_interface/move_group.h>

int main(int argc, char ** argv)
{
  //Initialise ROS
  ros::init(argc, argv, "testRobotMoveit");
  ros::NodeHandle nh("~");

  ros::AsyncSpinner spinner(1);
  spinner.start();

  iiwa_ros::iiwaRos my_iiwa;
  my_iiwa.init();

  std::string movegroup_name, ee_link;
  geometry_msgs::PoseStamped command_cartesian_position;

  nh.param<std::string>("move_group", movegroup_name, "manipulator");
  nh.param<std::string>("ee_link", ee_link, "tool_link_ee");

  double ros_rate;
  nh.param("ros_rate", ros_rate, 0.1);
  ros::Rate * loop_rate_ = new ros::Rate(ros_rate);

  int direction = 1;

  moveit::planning_interface::MoveGroup group(movegroup_name);
  moveit::planning_interface::MoveGroup::Plan myplan;

  group.setPlanningTime(0.5);
  group.setPlannerId("RRTConnectKConfigDefault");
  group.setEndEffectorLink(ee_link);

  bool success_plan = false;
  bool motion_done = false;
  bool new_pose   = false;

  printf("Got past the initalisation stuff\n");

  while (ros::ok())
  {
    //if (my_iiwa.getRobotIsConnected())
   // {
      command_cartesian_position = group.getCurrentPose(ee_link);

      command_cartesian_position.pose.position.z -= direction * 0.1;

      command_cartesian_position.pose.position.x -= direction * 0.1;

      group.setStartStateToCurrentState();

      group.setPoseTarget(command_cartesian_position, ee_link);

      success_plan = group.plan(myplan);

      if(success_plan)
      {
        motion_done = group.execute(myplan);
        ROS_INFO("Plan Succesfully Moved");
      }

      if (motion_done)
      {
        direction *= -1;
        loop_rate_->sleep();
      }
      else{
        ROS_WARN_STREAM("Robot is not connected..");
        ros::Duration(5.0).sleep();
      }
    //}
  }
 }