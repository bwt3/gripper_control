#include "ros/ros.h"
#include <string>
#include "ArduinoCommandDef.h"
#include <gripper_control/ArduinoCommand.h>
#include <gripper_control/ArduinoStatus.h>

#include <cstdio>
#include <ctime>

#define DISTANCE_PER_ROTATION 50
#define GRIPPER_STEPPER_SPEED_RPM 60
#define SOCKET_STEPPER_SPEED_RPM  10

#define GRIPPER_STEPPER_TIME_OUT 6

class AbstractGripper
{
  public:
    std::string serialPath = "/dev/ttyACM0"; //Default

    //Global ROS Arduino Publishers and Subscribers
    ros::Subscriber ros_arduino_sub;
    ros::Publisher ros_arduino_pub;
    //ROS Async Spinner
    ros::NodeHandle node;

    //CommandComplete
    bool commandCompleted = true;
    int  nutCurrentAngle = 0;
    int  gripperCurrentDistance = 200;

    AbstractGripper()
    {
    
    }

    //receivedArduinoStatus function is a call back which states whether a command has completed.
    void receivedArduinoStatus(const gripper_control::ArduinoStatus::ConstPtr &msg)
    {
      if (msg->current_command == WAIT_COMMAND)
      {
        commandCompleted = true;
      }

        printf("Current Command: %d\n",msg->current_command);

        printf("Nut Stepper Direction: %d\n", msg->nut_stepper_direction);
        printf("Nut Stepper Target Angle: %d\n", msg->nut_stepper_target_angle);
        printf("Nut Stepper Current Angle %d\n", msg->nut_stepper_current_angle);
        printf("Nut Stepper Speed: %d\n", msg->nut_stepper_speed);
        printf("Nut Stepper Rotations: %d\n", msg->nut_stepper_number_of_rotations_to_go);

        printf("Gripper Stepper Direction %d\n", msg->gripper_stepper_direction);
        printf("Gripper Stepper Target Angle %d\n", msg->gripper_stepper_target_angle);
        printf("Gripper Stepper Current Angle %d\n", msg->gripper_stepper_current_angle);
        printf("Gripper Stepper Speed %d\n", msg->gripper_stepper_speed);
        printf("Gripper Stepper Rotations still to complete %d\n", msg->gripper_stepper_number_of_rotations_to_go);
    
        nutCurrentAngle = msg->nut_stepper_current_angle;
    }


    //function which sets up gripper publisher and launches the 
    void init(ros::NodeHandle nh, std::string serialPath = "/dev/ttyACM0")
    {
      node = nh;
      std::string command = "rosrun rosserial_python serial_node.py ";

      command.append(serialPath);

      const char * commandChar = command.c_str();
      //Start the rosserial node
      system(commandChar);


      //Setup ROSSerial Publishers and Subscribers
      ros_arduino_sub = node.subscribe("PC_Arduino_TX", 1, &AbstractGripper::receivedArduinoStatus, this); // Buffersize is one 
      ros_arduino_pub = node.advertise<gripper_control::ArduinoCommand>("PC_Arduino_RX",20);
    
    }

    //Move Sotcket Position
    bool MoveSocketPosition(int angle, int direction)
    {
      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = 0;
      msg.gripper_stepper_angle     = angle;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((!commandCompleted) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    bool MoveSocketMultipleRotations(int angle, int direction, int rotations)
    {
      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = rotations;
      msg.gripper_stepper_angle     = angle;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((!commandCompleted) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    bool MoveGripperDistanceMM(int distance, int direction)
    {
      int numberOfRotations = distance / DISTANCE_PER_ROTATION;
      int leftOver   = distance % DISTANCE_PER_ROTATION;

      float angle  = leftOver / DISTANCE_PER_ROTATION * 360;
      int   angleInt = angle * 10;
      


      //Construct message

      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = numberOfRotations;
      msg.gripper_stepper_angle     = angleInt;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = GRIPPER_STEPPER_SPEED_RPM;

      //Send Message
      commandCompleted = false;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((!commandCompleted) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 60.0)
      {
        return false;
      }
      else
      {
        if (direction == CLOCKWISE_STEPPER_DIR)
        {
          gripperCurrentDistance = gripperCurrentDistance + distance;
        }
        else
        {
          gripperCurrentDistance = gripperCurrentDistance - distance;
        }
        return true;

      }
  
    }

    int getNutAngle()
    {
      return nutCurrentAngle;
    }
}; 



