#include "ros/ros.h"
#include <iiwa_ros.h>
#include <tf/transform_listener.h>
#include "geometry_msgs/PoseStamped.h"

geometry_msgs::PoseStamped current_position;

void printPosition(geometry_msgs::PoseStamped position)
{
    ROS_INFO("X: %f", position.pose.position.x);
    ROS_INFO("Y: %f", position.pose.position.y);
    ROS_INFO("Z: %f", position.pose.position.z);
    
    ROS_INFO("Angle\n");

    ROS_INFO("X: %f", position.pose.orientation.x);
    ROS_INFO("Y: %f", position.pose.orientation.y);
    ROS_INFO("Z: %f", position.pose.orientation.z);
    ROS_INFO("W: %f", position.pose.orientation.w);
    

}

void updatePosition(const geometry_msgs::PoseStamped::ConstPtr& currentPosition)
{
    current_position.header = currentPosition->header;
    current_position.pose.position.x = currentPosition->pose.position.x;
    current_position.pose.position.y = currentPosition->pose.position.y;
    current_position.pose.position.z = currentPosition->pose.position.z;
    current_position.pose.orientation.x = currentPosition->pose.orientation.x;
    current_position.pose.orientation.y = currentPosition->pose.orientation.y;
    current_position.pose.orientation.z = currentPosition->pose.orientation.z;
    current_position.pose.orientation.w = currentPosition->pose.orientation.w;
}

geometry_msgs::PoseStamped getPositionToMoveFromUser(geometry_msgs::PoseStamped current)
{

  geometry_msgs::PoseStamped tempPose;
  tf::Quaternion currentTFQ, q;
  geometry_msgs::Quaternion geo_q;
  float x,y,z,roll,pitch,yaw;

  //Translate the current quaternion into yaw pitch and roll
  geometry_msgs::PoseStamped currentPosition = current;

  ROS_INFO("The current position of robot end-effector is are x:%f, y:%f, z:%f\n", currentPosition.pose.position.x,currentPosition.pose.position.x,currentPosition.pose.position.z);
  
  tf::quaternionMsgToTF(currentPosition.pose.orientation, currentTFQ);

  tfScalar rollTF;
  tfScalar pitchTF;
  tfScalar yawTF;

  tf::Matrix3x3 convertedAngular(currentTFQ);
  convertedAngular.getRPY(rollTF, pitchTF, yawTF, 1); //Gets the roll pitch and yaw

  ROS_INFO("The angular position values are Roll %f Pitch %f Yaw %f\n", (double) rollTF, (double) pitchTF, (double) yawTF);

  printf("Please provide the prosition to move to:");
  printf("X:");
  scanf("%f", &x);

  printf("Y:");
  scanf("%f", &y);

  printf("Z:");
  scanf("%f", &z);

  printf("Please provide the roll, pitch and yaw for the end effector in radians\n");
  printf("Roll");
  scanf("%f", &roll);

  printf("Pitch");
  scanf("%f", &pitch);

  printf("Yaw");
  scanf("%f", &yaw);

  printf("The values read in were x:%f, y:%f, z:%f, roll:%f, pitch:%f, yaw:%f", x,y,z,roll,pitch,yaw);

  //Create Quaternion from the roll pitch and yaw
  q = tf::createQuaternionFromRPY(roll, pitch, yaw);
  q.normalize();
  tf::quaternionTFToMsg(q,geo_q);

  tempPose.pose.position.x = x;
  tempPose.pose.position.y = y;
  tempPose.pose.position.z = z;
  
  tempPose.pose.orientation = geo_q;

  return tempPose;
}


// Main for the ROS
int main(int argc, char ** argv)
{
    ros::init(argc, argv, "sampleMovement");
    ros::NodeHandle n;
    ros::WallRate loop_rate(500);
    
	ros::AsyncSpinner spinner(1);
    spinner.start();

    char delay;

    ROS_INFO("Setup ROS Correctly");
    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);


    //initialise the iiwa
    iiwa_ros::iiwaRos my_iiwa;
    my_iiwa.init();

    ROS_INFO("Init iiwa");
    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);

    //Subscribe to the position
    ros::Subscriber pos_sub = n.subscribe("/iiwa/state/CartesianPose",1, updatePosition);
    ros::Publisher  pos_pub = n.advertise<geometry_msgs::PoseStamped>("iiwa/command/CartesianPose",1);
    //Get current position of the robot


    if (my_iiwa.getCartesianPose(current_position))    
    {
        printPosition(current_position);
    }
    else
    {
        ROS_INFO("No Position Recevied by KUKA Program exiting");
        return 1;
    }


    ROS_INFO("Press any Key to continue....");
    scanf("%c", &delay);

    //Create the new position
    geometry_msgs::PoseStamped new_position;

    //Setting the robot to go very slowly
    my_iiwa.getPathParametersService().setJointRelativeVelocity(0.25);
    my_iiwa.getPathParametersService().setJointRelativeAcceleration(0.25);

    new_position = getPositionToMoveFromUser(current_position);


    //While Loop
    while (ros::ok())
    {
        pos_pub.publish(new_position);
        ros::spinOnce();
        
        //Get the time to the destintation
        double time = my_iiwa.getTimeToDestinationService().getTimeToDestination();
        ROS_INFO("Time to the destination is %f s\n", time);
        //while (time > 0 || time != -999)
        //{

        //    ros::spinOnce();
        //    time = my_iiwa.getTimeToDestinationService().getTimeToDestination();
        //}
            
        //if (time <= 0)
        //{
        //    ROS_INFO("KUKA reached it's destination in %f s", time);
        //}
        //else if (time == 999)
        //{
        //    ROS_INFO("KUKA encountered an error.");
        //    return 1;
        //}
        ros::spinOnce();
        loop_rate.sleep();
        ROS_INFO("Press any Key to continue....");
        scanf("%c", &delay);
        new_position = getPositionToMoveFromUser(current_position);
    }

return 0;
}