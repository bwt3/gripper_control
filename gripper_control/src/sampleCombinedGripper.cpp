#include "ros/ros.h"
#include <iiwa_ros.h>
#include <moveit/move_group_interface/move_group.h>
#include "ArduinoCommandDef.h"

//Timing Values
#define MOVEIT_PLANNING_TIME 1  //Time in Seconds to allow MoveIt to determine a position to move to
#define ROS_ASYNCSPINNER_THREADS 2

#define IMAGE false
#define SIMULATION true
#define ARDUINO_SERIAL_PATH "/dev/tty/ACM0"

ros::NodeHandle * nh;
#include "abstractGripper.h"

/*
ros::AsyncSpinner initROS(int argc, char ** argv)
{
  //Call and start the Async Spinner
  ros::AsyncSpinner spinner(ROS_ASYNCSPINNER_THREADS);
  spinner.start();


  return spinner;
}
*/

void waitForUser()
{
  char input;
  printf("Program Connected to Grippers and iiwa Robot\n");
  printf("Press any key to start\n");
  scanf("%c", &input);
}

/*
moveit::planning_interface::MoveGroup initMoveGroup(ros::NodeHandle node)
{
  group.setPlanningTime(MOVEIT_PLANNING_TIME);
  group.setPlannerId("RRTConnectKConfigDefault"); //Use the RRTConnect Planner
  group.setEndEffectorLink(ee_link);

  //Setup parameters within the code
  node.param<std::string>("move_group", movegroup_name, "manipulator");
  node.param<std::string>("ee_link", ee_link, "tool_link_ee");
  return group;
}

*/
geometry_msgs::PoseStamped getPositionToMoveFromUser(moveit::planning_interface::MoveGroup group)
{

  geometry_msgs::PoseStamped tempPose;
  tf::Quaternion currentTFQ, q;
  geometry_msgs::Quaternion geo_q;
  float x,y,z,roll,pitch,yaw;

  //Translate the current quaternion into yaw pitch and roll
  geometry_msgs::PoseStamped currentPosition = group.getCurrentPose();

  ROS_INFO("The current position of robot end-effector is are x:%f, y:%f, z:%f\n", currentPosition.pose.position.x,currentPosition.pose.position.x,currentPosition.pose.position.z);
  
  tf::quaternionMsgToTF(currentPosition.pose.orientation, currentTFQ);

  tfScalar rollTF;
  tfScalar pitchTF;
  tfScalar yawTF;

  tf::Matrix3x3 convertedAngular(currentTFQ);
  convertedAngular.getRPY(rollTF, pitchTF, yawTF, 1); //Gets the roll pitch and yaw

  ROS_INFO("The angular position values are Roll %f Pitch %f Yaw %f\n", (double) rollTF, (double) pitchTF, (double) yawTF);

  printf("Please provide the prosition to move to:");
  printf("X:");
  scanf("%f", &x);

  printf("Y:");
  scanf("%f", &y);

  printf("Z:");
  scanf("%f", &z);

  printf("Please provide the roll, pitch and yaw for the end effector in radians\n");
  printf("Roll");
  scanf("%f", &roll);

  printf("Pitch");
  scanf("%f", &pitch);

  printf("Yaw");
  scanf("%f", &yaw);

  printf("The values read in were x:%f, y:%f, z:%f, roll:%f, pitch:%f, yaw:%f", x,y,z,roll,pitch,yaw);

  //Create Quaternion from the roll pitch and yaw
  q = tf::createQuaternionFromRPY(roll, pitch, yaw);
  q.normalize();
  tf::quaternionTFToMsg(q,geo_q);

  tempPose.pose.position.x = x;
  tempPose.pose.position.y = y;
  tempPose.pose.position.z = z;
  
  tempPose.pose.orientation = geo_q;

  return tempPose;
}


geometry_msgs::PoseStamped getPositionToMoveTo(moveit::planning_interface::MoveGroup group )
{
  #if (IMAGE == true)
    return NULL;
  #else
    return getPositionToMoveFromUser(group);
    
  #endif
}


bool hasNutLockedOnFromUser()
{
  char result;
  printf("Has the socket went on over the nut. Y/n\n");
  scanf("%c", &result);

  if (result == 'Y')
  {
    return true;
  }
  else
  {
    return false; 
  }

}

//hasNutLockedOn is a function which determines whether the nut is within the socket
bool hasNutLockedOn()
{
  #if (IMAGE == true)
    //To Do
    return NULL;
  #else
    return hasNutLockedOnFromUser();
    
  #endif
}

//moveRobot method calls the correct methods to move the iiwa14 robot using
//the MoveIt Planning

bool moveRobot(moveit::planning_interface::MoveGroup group, geometry_msgs::PoseStamped positionToMoveTo, std::string end_link)
{
  bool plan_successful = false;
  bool move_successful = false;
  moveit::planning_interface::MoveGroup::Plan currentPlan;

  //Check whether we are connected to the KUKA if simulation is selected
  #if (SIMULATION == false)
    if (my_iiwa14.getRobotIsConnected())
    {ARDUINO_SERIAL_PATH
  #endif
  
  //Tell the MoveGroup that the current state is the start state
  group.setStartStateToCurrentState();

  //Tell the MoveGroup the target Cartesian Pose with 
  group.setPoseTarget(positionToMoveTo, end_link);

  //Produce a Plan
  plan_successful = group.plan(currentPlan);

  if (plan_successful)
  {
    ROS_INFO("Planning successful\n Execution Starting");
    move_successful = group.execute(currentPlan);

    if (move_successful)
    {
      ROS_INFO("MOVE SUCCESSFUL");
      return true;
    }
    
    return false;
  }

  
  #if (SIMULATION == false)
    }
    else
    {
      ROS_WARN("The iiwa Robot is not connected\n");
    }
  #endif
}


//Main function
int main(int argc, char ** argv)
{

  printf("Hello\n\n\n\n");
//Initialise ROS
  ros::init(argc, argv, "sampleCombinedGripper");
  ros::NodeHandle node("~");

  ros::AsyncSpinner spinner(1);
  spinner.start();

  double ros_rate;
  node.param("ros_rate", ros_rate, 0.5);
  ros::Rate * loop_rate_ = new ros::Rate(ros_rate);

  iiwa_ros::iiwaRos my_iiwa;
  my_iiwa.init();
  printf("Hello2\n\n\n\n");

  std::string movegroup_name, ee_link;

  node.param<std::string>("move_group", movegroup_name, "manipulator");
  node.param<std::string>("ee_link", ee_link, "tool_link_ee");

  
  moveit::planning_interface::MoveGroup group(movegroup_name);
  moveit::planning_interface::MoveGroup::Plan myplan;

  group.setPlanningTime(0.5);
  group.setPlannerId("RRTConnectKConfigDefault");
  group.setEndEffectorLink(ee_link);
    printf("Hello3\n\n\n\n");

  //Initialise the Abstract Arduino Gripper & Nut
  //initGripper();

  printf("Hello3\n\n\n\n");

  //Start User Input Loop
  while(!ros::ok())
  {
    ROS_WARN("PROBLEMS WITH ROS");
  }
  waitForUser();

  //Wait for Image to Calculate Point to Move End Effector to
  geometry_msgs::PoseStamped nutPosition = getPositionToMoveTo(group);
  printf("HelloGotData\n\n\n\n");
  //Then Move End Effector
  if (moveRobot(group, nutPosition, ee_link))
  {
    ROS_WARN("MoveIt unable to determine path to move to.\n Program terminating\n");
    return 1;
  }
/*
  //int nutCurrentAngle = gripper.getNutAngle();
  bool nutWithinSocket = hasNutLockedOn();
  //Then turn nut clockwise
  while (nutWithinSocket)
  {
    nutWithinSocket = hasNutLockedOn();
   // gripper.MoveSocketPosition((float) (nutCurrentAngle + 1.8), CLOCKWISE_STEPPER_DIR);

    //Move iiwa_robot_forward to check if new angle will accept the nut.
    //nutCurrentAngle = gripper.getNutAngle();

    //Check if the nut is within the socket
    nutWithinSocket = hasNutLockedOn();
  }

  //Move Grippers


  //Then Remove the wheel

  printf("Wheel Removal Successful\n");
  */
  return 0;


}