#include "ros/ros.h"
#include <gripper_control/ArduinoCommand.h>
#include <gripper_control/ArduinoStatus.h>
#include <string>

ros::NodeHandle * nh;

#include "abstractGripper.h"

void welcomeMessage();
bool nutRemoval();
bool exampleProcess();

int main(int argc, char ** argv)
{
  ros::init(argc,argv, "testLibrary");
  ros::NodeHandle node;
  nh = &node;
  char delay;

  ros::AsyncSpinner spinner(1);
  spinner.start();


  ros::spinOnce();

  initGripper();

  welcomeMessage();

  resetArduinoAngles();
  MoveGripperDistanceBetweenMM(262);  
  ros::Duration(10).sleep();
  
  //MoveGripperDistanceBetweenMM(300);
    ros::Duration(10).sleep();
  //MoveGripperDistanceBetweenMM(262);

  
  char spinDelay = 'Y';
  while (spinDelay != 'N')
  {
    printf("Turn nut 1 Rotation. Type Y to continue. N to Finish\n");
    scanf("%c", &spinDelay);
    if (spinDelay != 'N')
    {
      MoveSocketMultipleRotations(0.0,1,1);
    }

  }
  
  printf("Finish Turning Nut");
  scanf("%c", &delay);

  //MoveSocketMultipleRotations(20,1,1);

  //while (1)
  //{

  //}

}


void welcomeMessage()
{
  char input;

  ROS_INFO("Reset Grippers to Original Position Y/n\n");
  scanf("%c",&input);

  while (!input == 'Y')
  {
    ROS_INFO("Reset Grippers to Original Position Y/n\n");
    scanf("%c",&input);
  }

  ROS_INFO("Reset Arduino Values Y/n\n");
  scanf("%c", &input);
  if (input == 'Y')
  {
    resetArduinoAngles();
  }

}

bool exampleProcess()
{
  char delay;

  ROS_INFO("Move Grippers Out from Reset Position and disable Nut\n");
  scanf("%c", &delay);

  NutEnable(false);
  //{
  //  ROS_DEBUG("ARDUINO UNABLE TO COMPLETE COMMAND");
  //  ROS_DEBUG("PROGRAM TERMINATING");
  //  std::exit(EXIT_FAILURE);
  //  return false;
  //}  

  if (!MoveGripperDistanceBetweenMM(275))
  {
    ROS_DEBUG("ARDUINO UNABLE TO COMPLETE COMMAND");
    ROS_DEBUG("PROGRAM TERMINATING");
    std::exit(EXIT_FAILURE);
    return false;
  }  

  ROS_INFO("TODO Aquire 3D postion of wheel\n");
  ROS_INFO("TODO Move End Effector of Robot to Wheel\n");

  ROS_INFO("Move Grippers In Towards Wheel and then enable Nut Stepper\n");
  scanf("%c", &delay);
  if (!MoveGripperDistanceBetweenMM(260))
  {
    ROS_DEBUG("ARDUINO UNABLE TO COMPLETE COMMAND");
    ROS_DEBUG("PROGRAM TERMINATING");
    std::exit(EXIT_FAILURE);
    return false;
  } 

  NutEnable(true);
    //ROS_DEBUG("ARDUINO UNABLE TO COMPLETE COMMAND");
    //ROS_DEBUG("PROGRAM TERMINATING");
    //std::exit(EXIT_FAILURE);
    //return false;

  ROS_INFO("Is Socket Over Nut Y/n");
  scanf("%c", &delay);

  bool validCommand = false;

  while (!validCommand)
  {
    if (delay == 'Y')
    {
      validCommand = true;

      nutRemoval();
    
    }
    else if (delay == 'n')
    {
      //TODO: Workout what t odo if the bolt is not over the nut.
      ROS_INFO("Delay");
      validCommand = true;
    }
    else
    {
      
     scanf("%c", &delay);
    }
  }

  printf("Turn nut\n");
  scanf("%c", &delay);
  MoveSocketMultipleRotations(90.0,2,10);
  printf("Finished Turning \n");
  scanf("%c", &delay);
  MoveGripperDistanceBetweenMM(240); 
}

bool nutRemoval()
{
  ROS_INFO("Starting Nut Removal");
  return true;
}
