/*testComms.cpp
 Purpose: To provide a test program for testing ROS communications to an Arduino using
 the ROSSerial_Arduino Library.

 */

#include "ros/ros.h"
#include <stdlib.h>
#include <stdio.h>

//Include ArduinoROSMessages Defined
#include <gripper_control/ArduinoCommand.h>
#include <gripper_control/ArduinoStatus.h>
#include "ArduinoCommandDef.h"

//
void spinNutStepper();
void spinGripperStepper();
void setElectromagnet();
void displayArduinoStatus();
int  getCommandFromUser();

//Global ROS Publisher node
ros::Publisher ros_arduino_pub;

//Global ROS Subscriber node
ros::Subscriber ros_arduino_sub;

bool displayMessages = false;

//receivedArduinoStatus
void receivedArduinoStatus(const gripper_control::ArduinoStatus::ConstPtr &msg)
{
    printf("Display Messages: %d\n", displayMessages);
    if (displayMessages)
    {
        printf("Current Command: %d\n",msg->current_command);

        printf("Nut Stepper Direction: %d\n", msg->nut_stepper_direction);
        printf("Nut Stepper Target Angle: %d\n", msg->nut_stepper_target_angle);
        printf("Nut Stepper Current Angle %d\n", msg->nut_stepper_current_angle);
        printf("Nut Stepper Speed: %d\n", msg->nut_stepper_speed);
        printf("Nut Stepper Rotations: %d\n", msg->nut_stepper_number_of_rotations_to_go);

        printf("Gripper Stepper Direction %d\n", msg->gripper_stepper_direction);
        printf("Gripper Stepper Target Angle %d\n", msg->gripper_stepper_target_angle);
        printf("Gripper Stepper Current Angle %d\n", msg->gripper_stepper_current_angle);
        printf("Gripper Stepper Speed %d\n", msg->gripper_stepper_speed);
        printf("Gripper Stepper Rotations still to complete %d\n", msg->gripper_stepper_number_of_rotations_to_go);

        printf("Electromagnet On: %d\n",msg->electromagnet);
    }
    
}

//Main Function
//
int main(int argc, char ** argv)
{
    //Initialise ROS
    ros::init(argc,argv, "testComms");

    //connect to the rest of the ROS system
    ros::NodeHandle node;

    //Advertise to all nodes that this publisher
    ros_arduino_pub = node.advertise<gripper_control::ArduinoCommand>("/testLibrary/PC_Arduino_RX",20);

    //Subscriber buffer only contains the last sent meaning the the status is always up-to-date.
    ros_arduino_sub = node.subscribe("/testLibrary/PC_Arduino_TX",1,receivedArduinoStatus);


    while (getCommandFromUser() !=5)
    {
        ros::spinOnce();
    }
    printf("Shutting down testComms Publisher/Subscriber\n");
    return 0;
}




void spinNutStepper()
{
    int direction;
    int rotations;
    int angle;
    int speed;
    int enable;
    gripper_control::ArduinoCommand msg;

    printf("Specify Nut Stepper Direction: 1. Clockwise 2. Anticlockwise: ");
    scanf("%d", &direction);
    printf("Number of Rotations: ");
    scanf("%d", &rotations);
    printf("Specify Nut Stepper Angle: ");
    scanf("%d", &angle);
    printf("Specify Nut Stepper Speed: ");
    scanf("%d", &speed);
    printf("Stepper Enable 1: true 0: false: ");
    scanf("%d", &enable);

    //Create Command Message
    msg.current_command         = NUT_STEPPER_MOVEMENT_COMMAND;
    msg.nut_stepper_rotations   = rotations;
    msg.nut_stepper_angle       = angle;
    msg.nut_stepper_direction   = direction;
    msg.nut_stepper_speed       = speed;

    if (enable)
    {
    msg.nut_stepper_enable      = true;
    }
    else
    {
    msg.nut_stepper_enable      = false;  
    }

    //Send Message
    ros_arduino_pub.publish(msg);
}

void spinGripperStepper()
{
    int direction;
    int rotations;
    int angle;
    int speed;
    int enable;
    gripper_control::ArduinoCommand msg;

    printf("Specify Gripper Stepper Direction: 1. Clockwise 2. Anticlockwise: ");
    scanf("%d", &direction);
    printf("Number of Rotations: ");
    scanf("%d", &rotations);
    printf("Specify Gripper Stepper Angle: ");
    scanf("%d", &angle);
    printf("Specify Gripper Stepper Speed: ");
    scanf("%d", &speed);
    printf("Stepper Active 1: true 0: false: ");
    scanf("%d", &enable);

    //Create Command Message
    msg.current_command             = GRIPPER_STEPPER_MOVEMENT_COMMAND;
    msg.gripper_stepper_rotations   = rotations;
    msg.gripper_stepper_angle       = angle;
    msg.gripper_stepper_direction   = direction;
    msg.gripper_stepper_speed       = speed;
    if (enable)
    {
    msg.gripper_stepper_enable      = true;
    }
    else
    {
    msg.gripper_stepper_enable      = false;  
    }

 

    //Send Message
    ros_arduino_pub.publish(msg);
}

//setElectromagnet function takes in the user's input regarding the electromagnet
//from the keyboard and then publishs the required command to the Arduino topic Publisher 
void setElectromagnet()
{
    //Get Input from User
    int userChoice;
    gripper_control::ArduinoCommand msg;
    printf("Electromagnet 1. On, 0. Off:");
    scanf("%d",&userChoice);

    //Create new message
    msg.current_command = ELECTROMAGNET_COMMAND;
    msg.electromagnet   = userChoice;

    //Publish Message
    ros_arduino_pub.publish(msg);

}


//DisplayArduinoStatus function starts the status spinner which
//Activates the callback on the subscriber.

void displayArduinoStatus()
{
    int value = -1;
    printf("Press any key for the next set of messages Type 0 to stop");
    displayMessages = true;
    scanf("%d",&value);
    while(value != 0)
    {
        scanf("%d",&value);
        ros::spinOnce();
    }
    displayMessages = false;
}


int getCommandFromUser()
{
    int userSelection;
    printf("Please enter a command\n");
    printf("1. Move Nut Stepper\n");
    printf("2. Move Gripper Stepper\n");
    printf("3. Electromagnet\n");
    printf("4. Display Arduino Status\n");
    printf("5. Exit\n");
    scanf("%d",&userSelection);


        //Switch statement to assemble the correct message basd on the user's input
        switch (userSelection)
        {
            case 1:
                spinNutStepper();
                return 1;
                break;
            case 2:
                spinGripperStepper();
                return 2;
            case 3:
                setElectromagnet();
                return 3;
                break;
            case 4:
                displayArduinoStatus();
                return 4;
                break;
            case 5:
                return 5;
                break;
        }
}