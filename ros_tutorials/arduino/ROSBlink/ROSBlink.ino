#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

ros::NodeHandle nh;

void messageCallBack(const bool &msg)
{
  if (msg.data == true)
  {
    digitalWrite(13, HIGH);
  }
  else
  {
    digitalWrite(13, LOW);
  }
}

ros::Subscriber<std_msgs::Bool> sub("blink_topic", &messageCallBack);

void setup()
{
  nh.initNode();
  nh.subscribe(sub);
  pinMode(13,OUTPUT);

}

void loop()
{

  nh.spinOnce();
  delay(1);
}