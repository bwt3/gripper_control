#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Integer.h>

ros::NodeHandle nh;

char hello[13] = "Bonjourno!";

void messageCallBack(const std_msgs::Integer &msg)
{
  if (msg.data > 5)
  {
    digitalWrite(13, HIGH);
    delay(2);
    digitalWrite(13, LOW);
  }
}

void setup()
{
  nh.initNode();
  nh.advertise(chatter);
}

//ROS Subscriber set to listend for the topic attached and 
ros::Subscriber<std_msg::Integer> test_subscriber("float_topic", &messageCallBack);

void loop()
{
  nh.subscribe(test_subscriber);
  nh.spinOnce();
  delay(100);
}