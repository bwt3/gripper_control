#include "ros/ros.h"
#include "std_msgs/Bool.h"


int main(int argc, char **argv)
{
    ros::init(argc, argv, "blink");

    ros::NodeHandle n;

    //I.e. First parameter is the name of the topic and the second parameter is the size of the queue
    ros::Publisher publisher = n.advertise<std_msgs::Bool>("blink_topic",100);

    ros::Rate loop_rate(1);

    bool state = 0;

    while(ros::ok())
    {
        std_msgs::Bool msg;

        state = ~state;

        msg.data = state;

        publisher.publish(msg);

        ROS_INFO("%s %d","Sent msg", state);

        ros::spinOnce();
        loop_rate.sleep();

    }


}