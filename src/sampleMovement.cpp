#include "ros/ros.h"
#include <iiwa_ros.h>
#include <moveit/move_group_interface/move_group.h>


geometry_msgs::PoseStamped getRelativePositionFromUser(geometry_msgs::PoseStamped command_cartesian_position)
{
  geometry_msgs::PoseStamped tempPose;
  tf::Quaternion q;
  geometry_msgs::Quaternion geo_q;
  float x,y,z,roll,pitch,yaw;

  //Translate the current quaternion


  printf("The values currently are x:%f, y:%f, z:%f\n", command_cartesian_position.pose.position.x,command_cartesian_position.pose.position.x,command_cartesian_position.pose.position.z);
  
  //printf("The angular position values are Roll %f Pitch %f Yaw %f\n");

  printf("Please provide the relative prosition to move to:");
  printf("X:");
  scanf("%f", &x);

  printf("Y:");
  scanf("%f", &y);

  printf("Z:");
  scanf("%f", &z);

  printf("Please provide the roll, pitch and yaw for the end effector in radians\n");
  printf("Roll");
  scanf("%f", &roll);

  printf("Pitch");
  scanf("%f", &pitch);

  printf("Yaw");
  scanf("%f", &yaw);

  printf("The values read in were x:%f, y:%f, z:%f, roll:%f, pitch:%f, yaw:%f", x,y,z,roll,pitch,yaw);

  //Create Quaternion from the roll pitch and yaw
  q = tf::createQuaternionFromRPY(roll, pitch, yaw);
  q.normalize();
  tf::quaternionTFToMsg(q,geo_q);

  tempPose.pose.position.x += x;
  tempPose.pose.position.y += y;
  tempPose.pose.position.z += z;
  
  tempPose.pose.orientation = geo_q;

  return tempPose;
}


int main(int argc, char ** argv)
{
  //Initialise ROS
  ros::init(argc, argv, "testRobotMoveit");
  ros::NodeHandle nh("~");

  ros::AsyncSpinner spinner(1);
  spinner.start();

  double ros_rate;
  nh.param("ros_rate", ros_rate, 0.5);
  ros::Rate * loop_rate_ = new ros::Rate(ros_rate);

  iiwa_ros::iiwaRos my_iiwa;
  my_iiwa.init();

  std::string movegroup_name, ee_link;

  nh.param<std::string>("move_group", movegroup_name, "manipulator");
  nh.param<std::string>("ee_link", ee_link, "tool_link_ee");


  int direction = 1;

  moveit::planning_interface::MoveGroup group(movegroup_name);
  moveit::planning_interface::MoveGroup::Plan myplan;

  group.setPlanningTime(0.5);
  group.setPlannerId("RRTConnectKConfigDefault");
  group.setEndEffectorLink(ee_link);

  bool success_plan = false;
  bool motion_done = false;
  bool new_pose   = false;

  while (ros::ok())
  {
    //if (my_iiwa.getRobotIsConnected())
   // {
      geometry_msgs::PoseStamped command_cartesian_position;
      command_cartesian_position = group.getCurrentPose(ee_link);
      command_cartesian_position =  getRelativePositionFromUser(command_cartesian_position);


      group.setStartStateToCurrentState();

      group.setPoseTarget(command_cartesian_position, ee_link);

      success_plan = group.plan(myplan);

      if(success_plan)
      {
        motion_done = group.execute(myplan);

      }

      if (motion_done)
      {
        direction *= -1;
        loop_rate_->sleep();
      }
      else{
        ROS_WARN_STREAM("Robot is not connected..");
        ros::Duration(5.0).sleep();
      }
    //}
  }
 }