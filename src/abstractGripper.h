//abstractGripper.h


#ifndef ABSTRACTGRIPPER_H_
#define ABSTRACTGRIPPER_H_

#include "ros/ros.h"
#include <string>

#include <gripper_control/ArduinoCommand.h>
#include <gripper_control/ArduinoStatus.h>
#include "ArduinoCommandDef.h"

#include <cstdio>
#include <ctime>

#define DISTANCE_PER_ROTATION     89
#define INNER_DISTANCE_LIMIT      147        
#define OUTER_DISTANCE_LIMIT      281
#define GRIPPER_STEPPER_SPEED_RPM 20
#define SOCKET_STEPPER_SPEED_RPM  40

#define GRIPPER_STEPPER_TIME_OUT 6


//CommandComplete
int commandCompleted = 5;
int  nutCurrentAngle = 0;
int  gripperCurrentAngle = 0;
int  gripperCurrentDistance = INNER_DISTANCE_LIMIT;

bool gripperEnabled = true;
bool nutEnabled     = true;
ros::Subscriber ros_arduino_sub;
ros::Publisher  ros_arduino_pub;


    //receivedArduinoStatus function is a call back which states whether a command has completed.
    void receivedArduinoStatus(const gripper_control::ArduinoStatus::ConstPtr &msg)
    {
      if (msg->current_command == WAIT_COMMAND)
      {
        commandCompleted++;
      }

        //printf("Current Command: %d\n",msg->current_command);

        //printf("Nut Stepper Direction: %d\n", msg->nut_stepper_direction);
        //printf("Nut Stepper Target Angle: %d\n", msg->nut_stepper_target_angle);
        //printf("Nut Stepper Current Angle %d\n", msg->nut_stepper_current_angle);
        //printf("Nut Stepper Speed: %d\n", msg->nut_stepper_speed);
        //rintf("Nut Stepper Rotations: %d\n", msg->nut_stepper_number_of_rotations_to_go);

        //printf("Gripper Stepper Direction %d\n", msg->gripper_stepper_direction);
        //printf("Gripper Stepper Target Angle %d\n", msg->gripper_stepper_target_angle);
        //printf("Gripper Stepper Current Angle %d\n", msg->gripper_stepper_current_angle);
        //printf("Gripper Stepper Speed %d\n", msg->gripper_stepper_speed);
        //printf("Gripper Stepper Rotations still to complete %d\n", msg->gripper_stepper_number_of_rotations_to_go);
    
        nutCurrentAngle = msg->nut_stepper_current_angle;
        gripperCurrentAngle = msg->gripper_stepper_current_angle;
        gripperEnabled  = msg->gripper_stepper_enabled;
        nutEnabled      = msg->nut_stepper_enabled;

    }


    //initGripper sets up the publishers and subscribers to communicated with the Arduino
    void initGripper()
    {
      
        //Setup ROSSerial Publishers and Subscribers
        ros_arduino_sub = nh->subscribe("/testLibrary/PC_Arduino_TX", 1, &receivedArduinoStatus); // Buffersize is one 
        ros_arduino_pub = nh->advertise<gripper_control::ArduinoCommand>("/testLibrary/PC_Arduino_RX",20);
    }

    //Move Sotcket Position
    bool MoveSocketPosition(float angle, int direction)
    {
      int angleInt = (float) angle * 10;
      int noOfSteps = angle / 18;
      angleInt = noOfSteps * 18;
      
      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command           = NUT_STEPPER_MOVEMENT_COMMAND;
      msg.nut_stepper_rotations = 0;
      msg.nut_stepper_angle     = angleInt;
      msg.nut_stepper_direction = direction;
      msg.nut_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;
      msg.nut_stepper_enable    = true;

      //Send Message
      ros_arduino_pub.publish(msg);
      commandCompleted = 0;

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((commandCompleted < 5) && (duration < 30.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 30.0)
      {
        ROS_INFO("NUT STEPPER COMMAND NOT COMPLETED BY ARDUINO\n");
        return false;
      }
      else
      {
        return true;
      }
    }

    bool MoveSocketMultipleRotations(float angle, int direction, int rotations)
    {
      
      int angleInt = angle * 10;
      int noOfSteps = angle / 18;
      angleInt = noOfSteps * 18;

      printf("Angle %d\n",angleInt);
      printf("Direction %d\n", direction);
      printf("Rotations %d\n", rotations);

      //Construct message
      gripper_control::ArduinoCommand msg;
      msg.current_command       = NUT_STEPPER_MOVEMENT_COMMAND;
      msg.nut_stepper_rotations = rotations;
      msg.nut_stepper_angle     = angleInt;
      msg.nut_stepper_direction = direction;
      msg.nut_stepper_speed     = SOCKET_STEPPER_SPEED_RPM;
      msg.nut_stepper_enable    = true;

      //Send Message
      commandCompleted = 0;
      ros_arduino_pub.publish(msg);

      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;

      ros::spinOnce();
      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((commandCompleted < 5) && (duration < 60.0))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
        
      }

      if (duration > 60.0)
      {
        ROS_INFO("NUT STEPPER COMMAND NOT COMPLETED BY ARDUINO\n");
        return false;
      }
      else
      {
        return true;
      }
      
    }

    bool MoveGripperDistanceBetweenMM(int distance)
    {
      printf("Entered Loop\n");
      printf("Start Distance %d\n",gripperCurrentDistance);
      printf("Desired Distance %d\n", distance);
 
      
      int direction;
      //Check whether the distance is within the limits of the device
      if ((distance < INNER_DISTANCE_LIMIT) || (distance > OUTER_DISTANCE_LIMIT))
      {
        ROS_INFO("GRIPPERS: Distance Outwidth the bounds of the device.\n");
        return false;
      }
      int distanceToTravel = distance - gripperCurrentDistance;

      if (distanceToTravel < 0)
      {
        direction = ANTICLOCKWISE_STEPPER_DIR;
      }
      else
      {
        direction = CLOCKWISE_STEPPER_DIR;
      }

      printf("Distance To Travel %d\n", distanceToTravel);
      printf("distance per rotation %d\n",DISTANCE_PER_ROTATION);
      int numberOfRotations = distanceToTravel / DISTANCE_PER_ROTATION;
      
      int leftOver   = distanceToTravel % DISTANCE_PER_ROTATION;

      double differenceAngle  = (double) (leftOver / (double) DISTANCE_PER_ROTATION ) * 3600.0;
      int differenceAngleInt = (int) differenceAngle;
      int angleOverflows = (gripperCurrentAngle + differenceAngleInt) / 3600;
      int absoluteAngle = (gripperCurrentAngle + differenceAngleInt) % 3600;

      printf("AbsoluteAngle %d\n", absoluteAngle);
    
    
    
    
      if (absoluteAngle < 0 && direction == ANTICLOCKWISE_STEPPER_DIR)
      {
        absoluteAngle = 3600 + absoluteAngle;
      }

      int roundValue = absoluteAngle / 18;
      absoluteAngle = roundValue * 18;

      if (numberOfRotations < 0)
      {
        numberOfRotations = numberOfRotations * -1;
      }     

      gripper_control::ArduinoCommand msg;
      msg.current_command           = GRIPPER_STEPPER_MOVEMENT_COMMAND;
      msg.gripper_stepper_rotations = numberOfRotations;
      msg.gripper_stepper_angle     = absoluteAngle;
      msg.gripper_stepper_direction = direction;
      msg.gripper_stepper_speed     = GRIPPER_STEPPER_SPEED_RPM;
      msg.gripper_stepper_enable    = true;

      printf("Rotations %d\n", msg.gripper_stepper_rotations);
      printf("Angle %d\n", msg.gripper_stepper_angle);
      printf("Direction %d\n", msg.gripper_stepper_direction);
      printf("Speed %d\n", msg.gripper_stepper_speed);
      printf("Enabled %d", msg.gripper_stepper_enable);

      //Send Message
      ros_arduino_pub.publish(msg);
      commandCompleted = 0;

      printf("Published Command\n");


      //Get time
      clock_t startTime = clock();
      clock_t endTime = clock();
      double duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      
      //Now wait for the Arduino to return a status message stating
      //it has finished the instruction
      while ((duration < 30.0) && (commandCompleted < 5))
      {
        ros::spinOnce();
        endTime = std::clock();
        duration = (endTime - startTime) / (double) CLOCKS_PER_SEC;
      }

      if (duration > 30.0)
      {
        ROS_INFO("GRIPPER STEPPER COMMAND NOT COMPLETED BY ARDUINO\n");
        return false;
      }
      else
      {
        gripperCurrentDistance = gripperCurrentDistance + distanceToTravel;

        printf("Distance %d\n", gripperCurrentDistance);
        return true;

      }
    }

    int getNutAngle()
    {
      return nutCurrentAngle;
    }

    void GripperEnable(bool enabled)
    {
      if ((gripperEnabled && enabled) || (!gripperEnabled && !enabled))
      {
        return;
      }
      else
      {
        gripper_control::ArduinoCommand msg;
        msg.gripper_stepper_enable    = enabled;
        ros_arduino_pub.publish(msg);
      }
    }

    void NutEnable(bool enabled)
    {
      if ((nutEnabled && enabled) || (!nutEnabled && !enabled))
      {
        return;
      }
      else
      {
        gripper_control::ArduinoCommand msg;
        msg.gripper_stepper_enable    = enabled;
        ros_arduino_pub.publish(msg);
      }
    }

    void resetArduinoAngles()
    {
      gripper_control::ArduinoCommand msg;
      msg.electromagnet = true;

      commandCompleted = 0;
      while ((gripperCurrentAngle != 0) && (nutCurrentAngle !=0) && (commandCompleted < 5))
      {
        ros::spinOnce();
        ROS_INFO("Waiting for Arduino to Reset");
        ros::Duration(0.2).sleep();
      }
    }

#endif